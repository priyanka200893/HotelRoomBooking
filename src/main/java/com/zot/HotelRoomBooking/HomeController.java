package com.zot.HotelRoomBooking;

import java.awt.Checkbox;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.jws.WebParam.Mode;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import antlr.collections.List;

import com.zot.Admin.category.CategoryDAO;
import com.zot.Admin.category.CategoryEntity;
import com.zot.Admin.facility.FacilityDAO;
import com.zot.Admin.facility.FacilityEntity;
import com.zot.facilitymapping.FacilityMappingDAO;
import com.zot.facilitymapping.FacilityMappingEntity;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/admin")
public class HomeController {
	@Autowired
	private CategoryDAO categorydao;
	@Autowired
	private FacilityDAO facilitydao;
	@Autowired
	private FacilityMappingDAO facilityMappingdao;
	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */

//	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@RequestMapping(value = "/login1234", method = RequestMethod.POST)	
	public String loginPage(HttpServletRequest request) {
		ArrayList<FacilityEntity> roomlist = new ArrayList<FacilityEntity>();
		roomlist = (ArrayList<FacilityEntity>) facilitydao.populateFacilities();
		request.setAttribute("list", roomlist);
		return "Category";

	}

	@RequestMapping(value = "/cat")
	public String navigate(HttpServletRequest request,
			@ModelAttribute("facility") FacilityEntity facilityentity) {
		String location = request.getParameter("loc");
		try {
			ArrayList<FacilityEntity> list = new ArrayList<FacilityEntity>();
			list = (ArrayList<FacilityEntity>) facilitydao.populateFacilities();

			request.setAttribute("list", list);

			ArrayList<CategoryEntity> roomlist = (ArrayList<CategoryEntity>) categorydao
					.listCategory();
			request.setAttribute("roomlist", roomlist);

			System.out.print(location);
		} catch (Exception e) {
			System.out.print(e);
		}
		return location;
	}

//	@RequestMapping(value = "/", method = RequestMethod.GET)
	@RequestMapping(value = "/index", method = RequestMethod.GET)	
	public String home(
			@ModelAttribute("facility") FacilityEntity facilityentity,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			ArrayList<FacilityEntity> list = new ArrayList<FacilityEntity>();
			list = (ArrayList<FacilityEntity>) facilitydao.populateFacilities();
			request.setAttribute("list", list);
		} catch (Exception e) {
			System.out.print(e);
		}
		return "index";

	}

	@RequestMapping(value = "/addcategory", method = RequestMethod.POST)
	public String add(
			@Valid @ModelAttribute("category") CategoryEntity category,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model ,@RequestParam("images")MultipartFile file ) throws IOException 
	{
		
		ArrayList<FacilityEntity> list = new ArrayList<FacilityEntity>();
		list = (ArrayList<FacilityEntity>) facilitydao.populateFacilities();
		request.setAttribute("list", list);
		
		 
        category.setImages(file);
if("".equals(category.getImages()))
{
	model.addAttribute("img","please upload an image");
	return "Category";
}
		
else	if (result.hasErrors())
		{	
			System.out.print("result");
				if("".equals(category.getCategoryName()))
			{				
				model.addAttribute("catname", result.getFieldError("categoryName").getDefaultMessage());
			}
			else if("".equals(category.getDescrip()))
			{				
				model.addAttribute("desc", result.getFieldError("descrip").getDefaultMessage());
			}
		
			else 
			{
				model.addAttribute("cost", result.getFieldError("Cost").getDefaultMessage());
				
			}
		
		 return "Category";
		}
		
		
FacilityMappingEntity facilityMappingEntity = new FacilityMappingEntity();
String[] fid = request.getParameterValues("chk");
if(fid==null)
{
	
	model.addAttribute("map", "Please choose any one");
	return "Category";
}
else {
	
	String rs=categorydao.searchByName(category.getCategoryName().toUpperCase().trim());
	if(rs==null)
	{
		
		try {				
			String directory;
			String filename1=null;		
			filename1=request.getSession().getServletContext().getRealPath("\\resources\\");
			System.out.print(filename1);
			 directory="\\uploads\\";
			String filename=category.getCategoryName()+file.getOriginalFilename();
			String destination=filename1+directory+filename;
			System.out.println(":: destination path ::");
			System.out.println(destination);
			file.transferTo(new File(destination));				
			} catch (Exception e) {
				System.out.print(e);
				// TODO: handle exception
			}
		
int id = categorydao.addCategory(category);

System.out.print("id" + id);
for (int i = 0; i < fid.length; i++) {
	facilityMappingEntity.setCategory_id(id);
	facilityMappingEntity.setFacility_id(Integer.parseInt(fid[i]));
	facilityMappingdao.mapFacility(facilityMappingEntity);	
	
}
model.addAttribute("success","The Category Added successfully!!");

return "AddedCategory";
	}
if(rs!=null)
{
	model.addAttribute("msg",rs);
	return "Category";
	
} 


 }
return "Category";	
}

}

