package com.zot.Admin.roomsinfo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.zot.Admin.category.CategoryDAO;
import com.zot.Admin.category.CategoryEntity;

@Controller
@RequestMapping("/admin")
public class RoomsController {
	@Autowired
	private CategoryDAO categoryDAO;

	@Autowired
	private RoomsDAO roomsDaoImpl;

	@RequestMapping(value = "/addrooms", method = RequestMethod.POST)
	public String validaterooms(
			@Valid @ModelAttribute("rooms") RoomsEntity roomsEntity,
			BindingResult result1,
			@Valid @ModelAttribute("category") CategoryEntity categoryEntity,
			BindingResult result, HttpServletRequest request, Model model,
			@RequestParam("catid") String id) {
		    roomsEntity.setCategoryEntity(categoryEntity);

		ArrayList<CategoryEntity> list = (ArrayList<CategoryEntity>) categoryDAO
				.listCategory();
		model.addAttribute("roomlist", list);

		if ("default".equals(request.getParameter("catid"))) {
			
			model.addAttribute("catdef", "Please choose the category");
			return "AddRooms";
		} 
		else if (result1.hasErrors()) {
			 
			if ("".equals(roomsEntity.getRoomNos())) {
				model.addAttribute("num", result1.getFieldError("roomNos")
						.getDefaultMessage());
				return "AddRooms";
			} else {
				model.addAttribute("num1", result1.getFieldError("roomNos")
						.getDefaultMessage());
				return "AddRooms";
			}

		}

		else	 if (result.hasErrors()) {
			if ("".equals(roomsEntity.getCategoryEntity().getCatid())) {
				model.addAttribute("idnull", result.getFieldError("catid")
						.getDefaultMessage());
				return "AddRooms";
			}
		}

		
			String msg = roomsDaoImpl.searchRooms(roomsEntity
					.getCategoryEntity().getCatid());
			if (msg != "") {
				model.addAttribute("msg", msg);
				
			} else {
				roomsDaoImpl.saveRooms(roomsEntity);
				model.addAttribute("msg", "The rooms added successfully");
				return "RoomsDispatch";
			}

		

		return "AddRooms";
	}

	@RequestMapping(value = "/rooms")
	public String ViewRooms(Model model) {

		ArrayList<CategoryEntity> list = (ArrayList<CategoryEntity>) categoryDAO
				.listCategory();
		model.addAttribute("roomlist", list);

		ArrayList<RoomsEntity> getroomslist = (ArrayList<RoomsEntity>) roomsDaoImpl
				.getRooms();
		model.addAttribute("getroomslist", getroomslist);
		return "ViewRooms";

	}

	@RequestMapping("EditRooms")
	public String editRooms(HttpServletRequest request,
			@ModelAttribute("rooms") RoomsEntity roomsEntity, Model model) {

		ArrayList<CategoryEntity> list = (ArrayList<CategoryEntity>) categoryDAO.listCategory();
		model.addAttribute("roomlist", list);

		int rid = Integer.parseInt(request.getParameter("rid"));
		ArrayList<RoomsEntity> roomList = (ArrayList<RoomsEntity>) roomsDaoImpl
				.listRooms(rid);
		
		for (int i = 0; i < roomList.size(); i++) {
			model.addAttribute("roomid", roomList.get(i).getRoomNo());
			model.addAttribute("catid", roomList.get(i).getCategoryEntity()
					.getCatid());
			model.addAttribute("catname", roomList.get(i).getCategoryEntity()
					.getCategoryName());
			model.addAttribute("roomnos", roomList.get(i).getRoomNos());
		}

		return "EditRooms";

	}

	@RequestMapping(value = "/UpdateRoom")
	public String updateRooms(
			@Valid @ModelAttribute("rooms") RoomsEntity roomsEntity,
			BindingResult result1,
			@Valid @ModelAttribute("category") CategoryEntity categoryEntity,
			BindingResult result, Model model, HttpServletRequest request) {
		
		ArrayList<CategoryEntity> list = (ArrayList<CategoryEntity>) categoryDAO.listCategory();
		model.addAttribute("roomlist", list);

		ArrayList<RoomsEntity> roomList = (ArrayList<RoomsEntity>) roomsDaoImpl
				.listRooms(Integer.parseInt(request.getParameter("rcatid")));
		
		for (int i = 0; i < roomList.size(); i++) {
			
			model.addAttribute("roomid", roomList.get(i).getRoomNo());
			
			model.addAttribute("catid", roomList.get(i).getCategoryEntity()
					.getCatid());
			model.addAttribute("catname", roomList.get(i).getCategoryEntity()
					.getCategoryName());
			model.addAttribute("roomnos", roomList.get(i).getRoomNos());
		}

		roomsEntity.setCategoryEntity(categoryEntity);
		
		if ("default".equals(request.getParameter("catid"))) {

			model.addAttribute("catdef", "Please choose the category");
			return "EditRooms";
		}

		else if (result1.hasErrors()) {
			if ("".equals(roomsEntity.getRoomNos())) {
				model.addAttribute("num", result1.getFieldError("roomNos")
						.getDefaultMessage());
				return "EditRooms";
			} else {
				model.addAttribute("num1", result1.getFieldError("roomNos")
						.getDefaultMessage());
				return "EditRooms";
			}

		} else if (result.hasErrors()) {
			if ("".equals(roomsEntity.getCategoryEntity().getCatid())) {
				model.addAttribute("idnull", result.getFieldError("catid")
						.getDefaultMessage());
				return "EditRooms";
			}
			
		}
	
			
				int retid=roomsDaoImpl.searchRoomsByName(Integer.parseInt( request.getParameter("catid")));
				
				
				
				if((retid!=Integer.parseInt(request.getParameter("rcatid"))) && (retid!=0))
					
				{
					model.addAttribute("succ","the selected category has been added already");
					return "EditRooms";
				}
				else if((retid==Integer.parseInt(request.getParameter("rcatid"))) && (retid!=0)||(retid==0))
				{
					
					
					roomsEntity.setCategoryEntity(categoryEntity);
					roomsDaoImpl.UpdateRooms(roomsEntity);
						
					model.addAttribute("succ","Updated Sucessfully");
					return "RoomsDispatch";
				}
					
		
			
			
		return "";

		
	}
	@RequestMapping(value="DeleteRooms")
	public String DeleteRooms(HttpServletRequest request)
	{
		int cid=Integer.parseInt(request.getParameter("rid"));
		roomsDaoImpl.deleteRooms(cid);
		return "RoomsDispatch";
	}

}
