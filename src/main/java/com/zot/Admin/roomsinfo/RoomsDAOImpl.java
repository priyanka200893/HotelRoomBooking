package com.zot.Admin.roomsinfo;



import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLSyntaxErrorException;
import java.sql.SQLDataException;
import java.sql.SQLNonTransientConnectionException;
import com.zot.Admin.category.CategoryEntity;
@Repository
public class RoomsDAOImpl implements RoomsDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override	
	public void saveRooms(RoomsEntity roomsEntity) {
	
	 Session session1=sessionFactory.openSession();
	Transaction tx1=session1.beginTransaction();
	session1.save(roomsEntity);
	tx1.commit();
	session1.close();
		
	}

	@Override
	public String searchRooms(int id) {
		Session session2=sessionFactory.openSession();
		Transaction tx2=session2.beginTransaction();
		String sql="From RoomsEntity where categoryEntity.catid =:catid ";
		Query query=session2.createQuery(sql);
		query.setParameter("catid", id);
		List res=query.list();
		if(res.size()>0)
		{
			return "Sorry the rooms already added for the selected catgeory";
			
		}
		
		tx2.commit();
		session2.close();
		
		return "";
		
		
	}

@Override
	public List<RoomsEntity> getRooms() 
	{
		Session session3=sessionFactory.openSession();
		Transaction tx3=session3.beginTransaction();
		String sql="From RoomsEntity";
		Query query=session3.createQuery(sql);
		List list=query.list();
		tx3.commit();
		session3.close();
		return list;
	}

@Override
public List listRooms(int catId) {
	
	Session session4=sessionFactory.openSession();
	Transaction tx4=session4.beginTransaction();
	String sql="From RoomsEntity where categoryEntity.catid =:catid";
	Query query=session4.createQuery(sql);
	query.setParameter("catid", catId);
	
	System.out.println("CatId inside:"+catId);
	
	List list=query.list();
	
	tx4.commit();
	session4.close();
	
	return list;
}

@Override
public int searchRoomsByName(int id) {
	
	int retid=0;
	try{
		List<RoomsEntity> res;
		Session session5=sessionFactory.openSession();
		Transaction tx5=session5.beginTransaction();
		String sql="From RoomsEntity where categoryEntity.catid =:catid ";
		Query query=session5.createQuery(sql);
		query.setParameter("catid", id);
		res=query.list();
		 tx5.commit();
		 session5.close();
		 
		for (int i = 0; i < res.size(); i++) {
			retid=res.get(i).getCategoryEntity().getCatid();
		}
	}
	catch(Exception ex){
		ex.getMessage();
	}
	return retid;
}

@Override
public void UpdateRooms(RoomsEntity roomsEntity) {
	Session session6=sessionFactory.openSession();
	Transaction tx6=session6.beginTransaction();
	session6.update(roomsEntity);
	tx6.commit();
	session6.close();
}

@Override
public void deleteRooms(int cid) {
	
	RoomsEntity roomEntity=new RoomsEntity();
	Session session7=sessionFactory.openSession();
	Transaction tx7=session7.beginTransaction();	
	String sql="DELETE from CategoryEntity where catid =:catid" ;
	Query query=session7.createQuery(sql);
	query.setParameter("catid", cid);
	query.executeUpdate();
	tx7.commit();
	session7.close();
	
}
	
	

}
