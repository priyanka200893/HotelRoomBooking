package com.zot.Admin.roomsinfo;

import java.util.List;

public interface RoomsDAO {

	public void saveRooms(RoomsEntity roomsEntity);
	public String searchRooms(int id);
	public List  getRooms(); 
	public List listRooms(int catId);
	public int searchRoomsByName(int id);
	public void UpdateRooms (RoomsEntity roomsEntity);
	public void deleteRooms(int cid);
}
