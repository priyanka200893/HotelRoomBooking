package com.zot.Admin.roomsinfo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.zot.Admin.category.CategoryEntity;

@Entity
@Table(name="rooms")
public class RoomsEntity {

	@Id
	@Column(name="room_id")
	@GeneratedValue
	private String roomNo;
	
	@ManyToOne
	@JoinColumn(name="id",nullable=false)
	private CategoryEntity categoryEntity;
	public CategoryEntity getCategoryEntity() {
		
		return categoryEntity;
	}

	public void setCategoryEntity(CategoryEntity categoryEntity) {
	
		this.categoryEntity = categoryEntity;
	}
	
	
	@Pattern(regexp="^[0-9]{1,2}$",message="Please enter the valid number of rooms")
	@NotEmpty(message="cannot be empty")
	@Column(name="no_of_rooms")
	private String roomNos;
	
	public String getRoomNo() {
		return roomNo;
	}
	
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	
	public String getRoomNos() {
		return roomNos;
	}
	public void setRoomNos(String roomNos) {
		this.roomNos = roomNos;
	}
	
	
	
}
