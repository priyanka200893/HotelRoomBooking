package com.zot.Admin.review;


import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;







import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="review")
public class ReviewEntity {

	@Id
	@GeneratedValue
	@Column(name="rev_id")
	private int id;
	
	@Column(name="id")
	private int user_id;
	
	@NotEmpty(message="please enter the feedback")
	@Column(name="review")
	private String feedback;

	@OneToMany(mappedBy="reviewEntity",cascade=CascadeType.ALL)
	private List<ReviewApproval> reviews;
	

	public List<ReviewApproval> getReviews() {
		return reviews;
	}

	public void setReviews(List<ReviewApproval> reviews) {
		this.reviews = reviews;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	
	
	
	
	
}
