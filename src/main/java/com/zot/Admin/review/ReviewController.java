package com.zot.Admin.review;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ReviewController {

	@Autowired
	private ReviewDAO reviewDao;
	
	//@RequestMapping(value="/validate",method=RequestMethod.POST)
	@RequestMapping(value="/usrlinks/validate",method=RequestMethod.POST)
	public String validate(@Valid @ModelAttribute("review")ReviewEntity reviewEntity,BindingResult result,@ModelAttribute("revapproeve")ReviewApproval reviewApprovalEntity, Model model)
	{
	
		if(result.hasErrors())
		{
			
			if("".equals(reviewEntity.getFeedback()))
			{
				
			model.addAttribute("rev", result.getFieldError("feedback").getDefaultMessage());			
			return "AddReview";
			}
			
		}
		else
		{
			
		reviewDao.AddReview(reviewEntity);
		int id=reviewEntity.getId();
		reviewApprovalEntity.setReviewEntity(reviewEntity);
		reviewDao.AddReviewApproval(reviewApprovalEntity);
		model.addAttribute("success", "Thank you for your feedback!!");
	return	"FeedBackThnku";
		}
		
		return "AddReview";
		
	}
	
	// @RequestMapping(value="/reviews")
	@RequestMapping(value="/admin/reviews")
	public String change(Model model,HttpServletRequest request)
	{
		String searchStatus=request.getParameter("statusrequest");
	/*	ArrayList<ReviewApproval> list=(ArrayList<ReviewApproval>) reviewDao.pendingReviews(searchStatus);
    	model.addAttribute("reviewslist", list);
    	*/
    	model.addAttribute("style1", "hidden");
		// return "ReviewApproval";
    	return "ReviewApproval";
	}
	
	
	// @RequestMapping(value="/updateStatus")
	@RequestMapping(value="/admin/updateStatus")
	public String updateStatus(@ModelAttribute("reviewApproval")ReviewApproval reviewApproval,@ModelAttribute("review")ReviewEntity reviewEntity,Model model,HttpServletRequest request)
	{		
		String searchStatus=request.getParameter("statusrequest");
		ArrayList<ReviewApproval> list=(ArrayList<ReviewApproval>) reviewDao.pendingReviews(searchStatus);		
		model.addAttribute("reviewslist", list);
		
		if(request.getParameter("btnUpdate")!=null)
		{
			if("1".equals(request.getParameter("status")))
			{				
				model.addAttribute("err","Please choose the status");
				
				return "ReviewDispatch";
			}
			else {
			
			reviewApproval.setReviewEntity(reviewEntity);
			reviewDao.updateStatus(reviewApproval);
			ArrayList<ReviewApproval> list1=(ArrayList<ReviewApproval>) reviewDao.pendingReviews("all");		
			model.addAttribute("reviewslist", list1);
			return "ReviewApproval";
			
			}
		}
		else	if(request.getParameter("btnDelete")!=null)
		{
			int id=Integer.parseInt(request.getParameter("id"));	
			reviewDao.deleteStatus(id);
			
		}
		return "ReviewDispatch";
	}	
	@RequestMapping(value="/searchStatus")
	public String searchStatus(@ModelAttribute("reviewApproval")ReviewApproval reviewApproval,@ModelAttribute("review")ReviewEntity reviewEntity,Model model,HttpServletRequest request)
	{	
		String searchStatus=request.getParameter("statusrequest");
		ArrayList<ReviewApproval> list=(ArrayList<ReviewApproval>) reviewDao.pendingReviews(searchStatus);
			
		if(searchStatus.equals("1"))
			{				
				model.addAttribute("statuserr","Please choose the status");
				model.addAttribute("style1", "hidden");
			}
		
		else 	if(list==null)
		{
			model.addAttribute("records","No records found!!");
			model.addAttribute("style1", "hidden");						
		}
		else if(list.size()>0)
		{
		model.addAttribute("reviewslist", list);
		model.addAttribute("style1", "visible");				
	}							
	return "ReviewDispatch";
	}
	
	
	//@RequestMapping("/Usereviews")
	@RequestMapping("/usrlinks/Usereviews")
	public String UserReview(Model model)
	{		
		List<ReviewApproval> userReviews=reviewDao.getUserReviews(Integer.parseInt("1"));
		if(userReviews==null)
		{
			
			model.addAttribute("records","no records found");
			return "Norecords";
		}
		else
		{
		model.addAttribute("userReviewList", userReviews);
		return "UserReview";
		}
		
	}
	
	@RequestMapping(value="userviews",method=RequestMethod.POST)
	public String Update(Model model,HttpServletRequest request,@ModelAttribute("users") ReviewEntity reviewEntity)
	{
		
		List<ReviewApproval> userReviews=reviewDao.getUserReviews(Integer.parseInt("1"));
		model.addAttribute("userReviewList", userReviews);
	
		if(request.getParameter("UpdateFeedback")!=null)
		{
				 
			reviewDao.UpdateFeedback(reviewEntity);	
			return "ViewReviewsDispatch";
		}
			
		else if(request.getParameter("DeleteFeedback")!=null)
		{
			int id=Integer.parseInt(request.getParameter("id"));
			reviewDao.deleteStatus(id);
			return "ViewReviewsDispatch";
		}
		
		return "ViewReviewsDispatch";
	}
	
	
}
