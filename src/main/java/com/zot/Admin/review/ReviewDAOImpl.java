package com.zot.Admin.review;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.zot.Admin.category.CategoryEntity;

@Repository
public class ReviewDAOImpl implements ReviewDAO{

	@Autowired
	private SessionFactory sessionFactory;
	@Override
	
	public void AddReview(ReviewEntity reviewEntity) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();
		session.save(reviewEntity);
		tx.commit();
		session.close();
		
	}
	@Override
	public List<ReviewEntity> listReview() {
		Session session1=sessionFactory.openSession();
		Transaction tx1=session1.beginTransaction();
		String sql="From ReviewEntity";
		Query query=session1.createQuery(sql);
		List<ReviewEntity> list=query.list();
		return list;
	}
	@Override
	public void AddReviewApproval(ReviewApproval reviewApproval) {
		Session session2=sessionFactory.openSession();
		Transaction tx2=session2.beginTransaction();
		session2.save(reviewApproval);
		tx2.commit();
		session2.close();
		
	}
	@Override
	public List<ReviewApproval> pendingReviews(String searchStatus) {
		Session session3=sessionFactory.openSession();
		Transaction tx3=session3.beginTransaction();
		if("all".equals(searchStatus))
		{
			
			String sql3="From ReviewApproval";
			Query query3=session3.createQuery(sql3);
			List<ReviewApproval> list3=query3.list();
			if(list3.size()>0)
			{
				return list3;
			
			}
			else 
			{
				return null;
			}
		} 
		else 
		{
			
			String sql3="From ReviewApproval r where r.status =:status";
			Query query3=session3.createQuery(sql3);
			query3.setParameter("status", searchStatus);
			List<ReviewApproval> list3=query3.list();
			if(list3.size()>0)
			{
				return list3;
			
			}
			else 
			{
				return null;
			}
		}
	}
	
	@Override
	public void updateStatus(ReviewApproval reviewApproval) {
		Session session8=sessionFactory.openSession();
		Transaction tx8=session8.beginTransaction();
		session8.update(reviewApproval);
		tx8.commit();
		session8.close();
		
	}

	@Override
	public void deleteStatus(int id) {
		
		Session session_4=sessionFactory.openSession();
		Transaction tx_4=session_4.beginTransaction();	
		String sql4="DELETE from ReviewEntity where id =:id" ;
		Query query4=session_4.createQuery(sql4);
		query4.setParameter("id", id);
		query4.executeUpdate();
		tx_4.commit();
		session_4.close();
	}
	
	@Override
	public List<ReviewApproval> getUserReviews(int id) {
		
		Session session_5=sessionFactory.openSession();
		Transaction tx_5=session_5.beginTransaction();	
		String sql="From ReviewApproval r where r.user_id =:user_id " ;
		Query query5=session_5.createQuery(sql);
		query5.setParameter("user_id", id);
		//query5.setParameter("status", "pending");
	    List<ReviewApproval> list=query5.list(); 
		tx_5.commit();
		session_5.close();
		if(list.size()>0)
		{
			return list;
		}
		else 
		{
			return null;
		}
	}
	@Override
	public void UpdateFeedback(ReviewEntity reviewEntity) {
		Session session_6=sessionFactory.openSession();
		Transaction tx_6=session_6.beginTransaction();		
		session_6.update(reviewEntity);
		tx_6.commit();
		session_6.close();
	}
	
	@Override
	public void DeleteFeedBack(int id) {
		Session session_7=sessionFactory.openSession();
		Transaction tx_7=session_7.beginTransaction();	
		String sql7="DELETE from ReviewEntity where id =:id" ;
		Query query7=session_7.createQuery(sql7);
		query7.setParameter("id", id);
		query7.executeUpdate();
		tx_7.commit();
		session_7.close();
		
	}

}
