package com.zot.Admin.review;

import java.util.List;

public interface ReviewDAO {
public void AddReview(ReviewEntity reviewEntity);
public void AddReviewApproval(ReviewApproval reviewApproval);
public List<ReviewEntity> listReview();
public List<ReviewApproval> pendingReviews(String searchStatus);
public void updateStatus(ReviewApproval reviewApproval);
public void deleteStatus(int id);
public List<ReviewApproval> getUserReviews(int id);
public void UpdateFeedback(ReviewEntity reviewEntity);
public void DeleteFeedBack(int id);
}
