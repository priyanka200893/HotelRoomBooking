package com.zot.Admin.review;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.zot.Admin.category.CategoryEntity;

@Entity
@Table(name="review_approval")
public class ReviewApproval {

	@Id
	@GeneratedValue
	@Column(name="approval_id")
	 private int app_id;
	

	@Column(name="id")
	private int user_id;
	
	@Column(name="status")
	private String status;
	
	
	@Column(name="suggestion")
	private String message;
	
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name="edited_date")
	private String editeddate;

	@ManyToOne
	@JoinColumn(name="rev_id",nullable=false)
	private ReviewEntity reviewEntity;
	
	@Column(name="edited_time")
	private String editedtime;

	public ReviewEntity getReviewEntity() {
		return reviewEntity;
	}

	public void setReviewEntity(ReviewEntity reviewEntity) {
		this.reviewEntity = reviewEntity;
	}

	public int getApp_id() {
		return app_id;
	}



	public void setApp_id(int app_id) {
		this.app_id = app_id;
	}



	

	public int getUser_id() {
		return user_id;
	}



	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getEditeddate() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		editeddate=	dateFormat.format(date);
		return editeddate;
	}



	public void setEditeddate(String editeddate) {		
		this.editeddate = editeddate;
	}



	public String getEditedtime() {
		DateFormat dateFormat1 = new SimpleDateFormat("HH:mm:ss");
		Calendar calobj = Calendar.getInstance();
		editedtime=	dateFormat1.format(calobj.getTime());
		return editedtime;
	}

	public void setEditedtime(String editedtime) {
		this.editedtime = editedtime;
	}
	
	
	
	
	
}
