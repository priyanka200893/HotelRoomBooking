package com.zot.Admin.facility;

import java.util.List;

public interface FacilityDAO {
	
		public void addFacility(FacilityEntity facilityEntity);
		public List<FacilityEntity> populateFacilities();
		public void EditFacility(FacilityEntity facilityEntity);
		public List<FacilityEntity> searchById(int id);
		public void delete(int facid);
		public String searchByFacilityName(String facilityName);
		public String CheckName(String facilityName);
}
