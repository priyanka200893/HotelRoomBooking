package com.zot.Admin.facility;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;



@Repository
public class FacilityImpl implements FacilityDAO {

	@Autowired
	SessionFactory sessionFactory;
	@Override
	public void addFacility(FacilityEntity facilityEntity) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();
		session.save(facilityEntity);
		tx.commit();
		session.close();
					
	}
	
	@Override
	public List<FacilityEntity> populateFacilities() {
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();		
		String sql="From FacilityEntity";
		Query query=session.createQuery(sql);
		List<FacilityEntity> list =query.list();
		tx.commit();
		session.close();
		
		return list;
	}

	@Override
	public void EditFacility(FacilityEntity facilityEntity) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();	
		session.update(facilityEntity);
		tx.commit();
		session.close();
		
	}

	@Override
	public List<FacilityEntity> searchById(int id) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();		
		String sql="From FacilityEntity where facid =:id";
		Query query=session.createQuery(sql);
		query.setParameter("id", id);
		List<FacilityEntity> list =query.list();
		tx.commit();
		session.close();
		
		return list;
	}

	@Override
	public void delete(int facid) {
		FacilityEntity facilityEntity= new FacilityEntity();
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();		
		String sql="DELETE from FacilityEntity where facid =:facid" ;
		Query query=session.createQuery(sql);
		query.setParameter("facid", facid);
		query.executeUpdate();	
		tx.commit();
		session.close();
		
	}

	@Override
	public String searchByFacilityName(String facilityName) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();		
		String sql="From FacilityEntity where trim(upper(facname)) =:facname";
		Query query=session.createQuery(sql);
		query.setParameter("facname", facilityName);
		List list =query.list();
		if(list.size()>0)
		{
			return "Sorry the name already exists";
			
		}
		else
		{
		return null;
		}
	}

	@Override
	public String CheckName(String facilityName) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();		
		String sql="From FacilityEntity where trim(upper(facname)) =:facname";
		Query query=session.createQuery(sql);
		query.setParameter("facname", facilityName);
		List<FacilityEntity> list =query.list();
		String rs="";
		for(int i=0;i<list.size();i++)
		{
			rs=list.get(i).getFacname();
		}
		return rs;
	}

}
