package com.zot.Admin.facility;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="facilities")
public class FacilityEntity {

	@Id
	@GeneratedValue
	@Column(name="id")
	private int facid;
	
	@NotEmpty(message="Facility Name cannot be empty")
	@Column(name="description")
	private String facname;
	
	
	public int getFacid() {
		return facid;
	}
	public void setFacid(int facid) {
		this.facid = facid;
	}
	public String getFacname() {
		return facname;
	}
	public void setFacname(String facname) {
		this.facname = facname;
	}
	

}
