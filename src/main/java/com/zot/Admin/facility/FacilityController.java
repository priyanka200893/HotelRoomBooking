package com.zot.Admin.facility;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin")
public class FacilityController {
	
	@Autowired
	private FacilityDAO facilityDAO;
	
    @RequestMapping(value="addfacility", method = RequestMethod.POST)
	public String addFac(@Valid @ModelAttribute("facility") FacilityEntity facility,BindingResult result,@RequestParam("facname")String facilityName,Model model)		
	{	
	if(result.hasErrors())
	{
		if(facilityName.equals(""))
		{
			model.addAttribute("facname",result.getFieldError("facname").getDefaultMessage());
			
		}
		return "Facility";	
	}
	else
	{
	String facname=facilityDAO.searchByFacilityName(facilityName.toUpperCase().trim());
	if(facname!=null)
	{
		model.addAttribute("msg",facname);
		return "Facility";
	}
	if(facname==null)
	{
	facilityDAO.addFacility(facility);
	model.addAttribute("success","Facility added successfully!!");
	return "AddedFacilites";
	}
	
	}
	return "Facility";
	}

@RequestMapping(value="viewfacility")
public String viewFacility(HttpServletRequest request)
{
	ArrayList<FacilityEntity> list =  (ArrayList<FacilityEntity>) facilityDAO.populateFacilities();
	request.setAttribute("list", list);
	return "ViewFacility";
}

@RequestMapping(value="edit")
public String edit(HttpServletRequest request,@ModelAttribute("facilityEdit")FacilityEntity facilityEntity,Model model)
{
	
int fid= Integer.parseInt( request.getParameter("fid"));
ArrayList<FacilityEntity> facilityEntityList=	(ArrayList<FacilityEntity>) facilityDAO.searchById(fid);
for(int i=0;i<facilityEntityList.size();i++)
{
	model.addAttribute("facid", facilityEntityList.get(i).getFacid());
	model.addAttribute("facname", facilityEntityList.get(i).getFacname());
}
//request.setAttribute("facilityList",facilityEntityList );
	return "EditFacility";
}


@RequestMapping(value="/UpdateFacility",method=RequestMethod.POST)
public String UpdateFacility(@Valid @ModelAttribute("facility")FacilityEntity facilityEntity,BindingResult result,@RequestParam("facname")String facilityName,Model model,HttpServletRequest request)
{
	int fid= Integer.parseInt( request.getParameter("facid"));
	ArrayList<FacilityEntity> facilityEntityList=	(ArrayList<FacilityEntity>) facilityDAO.searchById(fid);
	for(int i=0;i<facilityEntityList.size();i++)
	{
		model.addAttribute("facid", facilityEntityList.get(i).getFacid());
		model.addAttribute("facname", facilityEntityList.get(i).getFacname());
	}
	
	
	if(result.hasErrors())
	{
		if(facilityName.equals(""))
		{
			
			model.addAttribute("facnameerror",result.getFieldError("facname").getDefaultMessage());					
		}
		return "EditFacility";	
	}
	
	else {
		 String rs= facilityDAO.CheckName(request.getParameter("facname").toUpperCase()).toUpperCase();
		 
		 
		if((!rs.equals(request.getParameter("checkfacname").toUpperCase())) && (!"".equals(rs)))
		 {
			
			 model.addAttribute("msg", "Sorry the name already exists");
			 return "EditFacility";
			
		 }
		else if((rs.equals(request.getParameter("checkfacname").toUpperCase())) && ((!"".equals(rs)))||("".equals(rs)))
		{					
facilityDAO.EditFacility(facilityEntity);
	return "FacilityDispatch";
	}
		return "FacilityDispatch";
	}
}

@RequestMapping(value="delete")
public String delete(@ModelAttribute("facility")FacilityEntity facilityEntity,HttpServletRequest request)
{
	int fid= Integer.parseInt( request.getParameter("fid"));
	facilityDAO.delete(fid);
	return "FacilityDispatch";
}
}
