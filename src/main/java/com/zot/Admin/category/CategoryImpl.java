 package com.zot.Admin.category;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import com.zot.Admin.facility.FacilityEntity;

@Repository
public class CategoryImpl implements CategoryDAO {
	@Autowired
	SessionFactory sessionFactory;

	
	@Override
	public int addCategory(CategoryEntity categoryentity) {		
		
		Session session_1=sessionFactory.openSession();
		Transaction tx_1=session_1.beginTransaction();		
		session_1.save(categoryentity);
		int id=categoryentity.getCatid();
		tx_1.commit();
		session_1.close();
        return id;
		
	}


	@Override
	public List<CategoryEntity> editCategory(int id) {
		
		Session session_2=sessionFactory.openSession();
		Transaction tx_2=session_2.beginTransaction();		
		String sql="From CategoryEntity c where c.catid =:id" ;
		Query query=session_2.createQuery(sql);
		query.setParameter("id", id);
		List<CategoryEntity> list =query.list();
		tx_2.commit();
		session_2.close();
		return list;
		
	}
	@Override
	public List<CategoryEntity> listCategory() {
		
		List<CategoryEntity> list = null;
		try{
			Session session_3=sessionFactory.openSession();
			Transaction tx_3=session_3.beginTransaction();		
			String sql="From CategoryEntity";
			Query query=session_3.createQuery(sql);
			 list =query.list();
			tx_3.commit();
			session_3.close();
		}
		catch(Exception ex){
			
			System.out.println(ex.getMessage());
			
		}
		return list;
	}


	@Override
	public void updateCatgeory(CategoryEntity category) {
		
		Session session_4=sessionFactory.openSession();
		Transaction tx_4=session_4.beginTransaction();			
		session_4.update(category);
		tx_4.commit();
		session_4.close();
	}


	@Override
	public void DeleteCategory(int id) {
		
		CategoryEntity categoryEntity=new CategoryEntity();
		Session session_5=sessionFactory.openSession();
		Transaction tx_5=session_5.beginTransaction();	
		String sql="DELETE from CategoryEntity where catid =:catid" ;
		Query query=session_5.createQuery(sql);
		query.setParameter("catid", id);
		query.executeUpdate();
		tx_5.commit();
		session_5.close();
		
	}


	@Override
	public String searchByName(String categoryName) {
		CategoryEntity categoryEntity=new CategoryEntity();
		Session session_6=sessionFactory.openSession();
		Transaction tx_6=session_6.beginTransaction();	
		String sql="From CategoryEntity Where trim(upper(categoryName)) =:catname";
		Query query=session_6.createQuery(sql);
		query.setParameter("catname",categoryName.toUpperCase());
		List resultSet=query.list();
		if(resultSet.size()>0)
		{
			return "Sorry the category already exists";
		}
		
		tx_6.commit();
		session_6.close();
		
		return null;
	}


	@Override
	public String searchbycatname(String categoryName) {
		
		String catname="";
		CategoryEntity categoryEntity=new CategoryEntity();
		Session session_7=sessionFactory.openSession();
		Transaction tx_7=session_7.beginTransaction();	
		String sql="From CategoryEntity Where trim(upper(categoryName)) =:catname";
		Query query=session_7.createQuery(sql);
		query.setParameter("catname",categoryName.toUpperCase());
		List<CategoryEntity> resultSet=query.list();
		for(int i=0;i<resultSet.size();i++)
		{
			catname=resultSet.get(i).getCategoryName();
		}
		
		tx_7.commit();
		session_7.close();
		
		return catname;
	}


	@Override
	public List<CategoryEntity> searchById(int categoryid) {
		// TODO Auto-generated method stub
		
		CategoryEntity categoryEntity=new CategoryEntity();
		Session session_8=sessionFactory.openSession();
		Transaction tx_8=session_8.beginTransaction();	
		String sql="From CategoryEntity Where id =:categoryid";
		Query query=session_8.createQuery(sql);
		query.setParameter("categoryid",categoryid);
		List<CategoryEntity> resultSet=query.list();
		
		tx_8.commit();
		session_8.close();
		
		return resultSet;
	}

	

}
