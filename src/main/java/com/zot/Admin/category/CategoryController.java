package com.zot.Admin.category;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.IOUtils;
import org.hibernate.ejb.criteria.expression.function.TrimFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.zot.Admin.facility.FacilityDAO;
import com.zot.Admin.facility.FacilityEntity;
import com.zot.facilitymapping.FacilityMappingDAO;
import com.zot.facilitymapping.FacilityMappingEntity;

@Controller
@RequestMapping("/admin")
public class CategoryController {
	@Autowired
	private CategoryDAO categorydao;
	@Autowired
	private FacilityDAO facilityDao;
	@Autowired
	private FacilityMappingDAO facilityMappingDao;

	@RequestMapping(value = "ListCategory")
	public ModelAndView ListCatgeory() {
		ModelAndView model = new ModelAndView("ViewCategory");
		ArrayList<CategoryEntity> list = (ArrayList<CategoryEntity>) categorydao
				.listCategory();
		model.addObject("listCategory", list);
		return model;
	}

	@RequestMapping(value = "Edit")
	public String EditCategory(HttpServletRequest request,
			@ModelAttribute("category") CategoryEntity category, Model model) {
		String id = request.getParameter("id");
		int convid = Integer.parseInt(id);
		ArrayList<CategoryEntity> list = (ArrayList<CategoryEntity>) categorydao
				.editCategory(convid);
		model.addAttribute("list", list);

		ArrayList<FacilityEntity> list1 = new ArrayList<FacilityEntity>();
		list1 = (ArrayList<FacilityEntity>) facilityDao.populateFacilities();
		request.setAttribute("list1", list1);

		ArrayList<FacilityMappingEntity> selectedFacilities = new ArrayList<FacilityMappingEntity>();
		selectedFacilities = (ArrayList<FacilityMappingEntity>) facilityMappingDao
				.selectedFacility(convid);
		
		request.setAttribute("selected", selectedFacilities);
		return "EditForm";
	}

	@RequestMapping(value = "Updatecategory", method = RequestMethod.POST)
	public String Updatecategory(
			@Valid @ModelAttribute("category") CategoryEntity category,
			BindingResult result, Model model, HttpServletRequest request,@ModelAttribute("mapping")FacilityMappingEntity facilityMappingEntity,@RequestParam("images") MultipartFile file ) throws IOException {
		try {				
			String directory;
			String filename1=null;		
			filename1=request.getSession().getServletContext().getRealPath("\\resources\\");
			System.out.print(filename1);
			 directory="\\uploads\\";
			String filename=category.getCategoryName()+file.getOriginalFilename();
			String destination=filename1+directory+filename;
			file.transferTo(new File(destination));				
			} catch (Exception e) {
				System.out.print(e);
				// TODO: handle exception
			}

		
		
		if(file.getOriginalFilename()!="")
		{
			
			category.setImages(file);
			
		}
		
		if(file.getOriginalFilename()=="")
		{
			
			
			String str=request.getParameter("upload");
			File filename= new File(str);
		    DiskFileItem fileItem = new DiskFileItem("file", "text/plain", false, filename.getName(), (int) filename.length() , filename.getParentFile());
		    fileItem.getOutputStream();
		    MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
		    category.setImages(multipartFile);
		}
			
		String id = request.getParameter("catid");
		int convid = Integer.parseInt(id);
		ArrayList<CategoryEntity> list = (ArrayList<CategoryEntity>) categorydao
				.editCategory(convid);
		model.addAttribute("list", list);

		
		
		
		ArrayList<FacilityEntity> list1 = new ArrayList<FacilityEntity>();
		list1 = (ArrayList<FacilityEntity>) facilityDao.populateFacilities();
		request.setAttribute("list1", list1);

		
		ArrayList<FacilityMappingEntity> selectedFacilities = new ArrayList<FacilityMappingEntity>();
		selectedFacilities = (ArrayList<FacilityMappingEntity>) facilityMappingDao
				.selectedFacility(convid);
		
		request.setAttribute("selected", selectedFacilities);
		
		
		
		 if (result.hasErrors()) 
		 {
			if ("".equals(category.getCategoryName())) {
				
				model.addAttribute("catname",
						result.getFieldError("categoryName")
								.getDefaultMessage());
				
			}
			
			else	if ("".equals(category.getDescrip())) {

				model.addAttribute("desc", result.getFieldError("descrip")
						.getDefaultMessage());
			}
			
			else 
			{
				model.addAttribute("costexp", result.getFieldError("Cost")
						.getDefaultMessage());
			}
			

			return "EditForm";
		}
		
		else {
			
			 String rs=categorydao.searchbycatname(request.getParameter("categoryName").toUpperCase()).toUpperCase();
		
		
			if((!rs.equals(request.getParameter("checkname").toUpperCase())) && (!"".equals(rs)))
			 {
				
				 model.addAttribute("msg", "Sorry the name already exists");
				 return "EditForm";
				
			 }
			 
		/*	 if((rs.equals(request.getParameter("categoryName"))&&  "".equals(rs) ))
			 
			 {
			 */
			else if((rs.equals(request.getParameter("checkname").toUpperCase())) && ((!"".equals(rs)))||("".equals(rs)))
		{
			
			
				 if(rs!=request.getParameter("categoryName")){
					 
					 String chk[]=request.getParameterValues("chk");
						
						if(chk==null)
						{
							System.out.print("chk null");
							model.addAttribute("chk","please choose any one");
							return "EditForm";
							
						}
						else 
						{
							
							
						categorydao.updateCatgeory(category);
						//facilityMappingDao.Update(facilityMappingEntity);
						
						facilityMappingDao.delete(convid);
						System.out.print(convid);
					for(int i=0;i<chk.length;i++)
						{
							
							facilityMappingEntity.setCategory_id(convid);
							facilityMappingEntity.setFacility_id(Integer.parseInt( chk[i]));
							facilityMappingDao.Update(facilityMappingEntity);
							
						} 
						return "EditCategory";
						}
						 
				 }
	
					}
					
			// }
			 
		
				 return "EditCategory";
			 }
			 
			
			
	}
	
	@RequestMapping(value = "Delete")
	public String Deletecategory(
			@ModelAttribute("category") CategoryEntity category,
			HttpServletRequest request) {
		String id = request.getParameter("id");
		int convid = Integer.parseInt(id);
		categorydao.DeleteCategory(convid);
		return "EditCategory";

	}
	
	
	
	

}
