package com.zot.Admin.category;

import java.io.IOException;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import com.mysql.jdbc.Blob;
import com.zot.Admin.roomsinfo.RoomsEntity;
import com.zot.facilitymapping.FacilityMappingEntity;

@Entity
@Table(name="category")

public class CategoryEntity {
	@Id 
	@GeneratedValue
	@Column(name="id")
	private int catid;
	
	@NotEmpty(message="Please enter the category name")
	@Column(name="room_type")
	private String categoryName;
	


@Pattern(regexp="^[0-9]+([.][0-9]{1,2})$",message="please enter the valid cost")
	@Column(name="cost")
	private String Cost;
	

@NotEmpty(message="Please enter the Description")
	@Column(name="description")
	private String descrip;


@Column(name="images")
private String images;


public String getImages() {
	return images;
}


public void setImages(MultipartFile file) {
	this.images =file.getOriginalFilename();
}

	@OneToMany(mappedBy="categoryEntity",cascade=javax.persistence.CascadeType.ALL)
	private Set<RoomsEntity> rooms;
	

	public String getDescrip() {
		return descrip;
	}

	

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}
	public int getCatid() {
		return catid;
	}
	public void setCatid(int catid) {
		this.catid = catid;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCost() {
		return Cost;
	}

	public void setCost(String cost) {
		this.Cost = cost;
	}



	public Set<RoomsEntity> getRooms() {
		return rooms;
	}



	public void setRooms(Set<RoomsEntity> rooms) {
		this.rooms = rooms;
	}

	


	

	
	
	
}
