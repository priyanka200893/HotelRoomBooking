package com.zot.Admin.category;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface CategoryDAO {
	
			public int addCategory(CategoryEntity categoryentity);
			public List<CategoryEntity>  editCategory(int id);
			public List<CategoryEntity> listCategory();
			public void updateCatgeory(CategoryEntity category);
			public void DeleteCategory(int id);
			
			public String searchByName(String categoryName);
			
			public String searchbycatname(String categoryName);
			
			public List<CategoryEntity> searchById(int categoryid);
			
}
