package com.zot.users.account.service;

import com.zot.users.account.model.Role;
import com.zot.users.account.model.User;
import com.zot.users.account.repository.RoleRepository;
import com.zot.users.account.repository.UserDAO;
import com.zot.users.account.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
	
   // @Autowired
   // private UserRepository userRepository;
    
  //  @Autowired
  //  private RoleRepository roleRepository;
    
    @Autowired
    private UserDAO userdao;
    
   
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Override
    public void save(User user) {
    	
      System.out.println("::UserName:: "+user.getUsername());
    	
      user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        
    //  HashSet<Role> roles = new HashSet<Role>(roleRepository.findAll());
      
     HashSet<Role> roles = new HashSet<Role>(userdao.findAllRoles());
     
     System.out.println("roles");
     System.out.println(roles);
     
      user.setRoles(roles);   
      // userRepository.save(user);
      userdao.save(user);
    }

    @Override
    public User findByUsername(String username) {
    	
       // return userRepository.findByUsername(username);
    	
    	 System.out.println("--UN--"+username);
    	
    	return userdao.findByUsername(username);
    }

   
}
