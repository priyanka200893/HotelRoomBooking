package com.zot.users.account.service;


import com.zot.users.account.model.User;

public interface UserService {
	
    void save(User user);

    User findByUsername(String username);
    
}
