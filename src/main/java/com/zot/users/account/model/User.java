package com.zot.users.account.model;


import javax.persistence.*;

import com.zot.Admin.review.ReviewEntity;

import java.util.Set;

@Entity
@Table(name = "user")
public class User {
	
    private Long id;
    private String username;
    private String password;
    private String passwordConfirm;
    private String email;
    private Set<Role> roles;
    
    // private Set<ReviewEntity> reviews;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
    	System.out.println("get id: "+id);
        return id;
    }

    public void setId(Long id) {
    	System.out.println("set id: "+id);
    	this.id = id;
    }

    public String getUsername() {
    	System.out.println("get username: "+username);
        return username;
    }

    public void setUsername(String username) {
    	System.out.println("set username: "+username);
    	this.username = username;
    }

    public String getPassword() {
    	System.out.println("get password: "+password);
        return password;
    }
    
    public String getEmail() {
    	System.out.println("get email: "+email);
		return email;
	}

	public void setEmail(String email) {
		System.out.println("set email: "+email);
		this.email = email;
	}

	public void setPassword(String password) {
		System.out.println("set password: "+password);
        this.password = password;
    }

    @Transient
    public String getPasswordConfirm() {
    	System.out.println("get pwd confirm: "+passwordConfirm);
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
    	System.out.println("set pwd confirm: "+passwordConfirm);
        this.passwordConfirm = passwordConfirm;
    }

    @ManyToMany
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    public Set<Role> getRoles() {
    	System.out.println("--get roles--");
    	System.out.println(roles);
        return roles;
    }

    public void setRoles(Set<Role> roles) {
    	System.out.println("--set roles--");
    	System.out.println(roles);
        this.roles = roles;
    }
    
    /*
	@OneToMany(fetch = FetchType.LAZY, mappedBy="user")
    public Set<ReviewEntity> getReviews() {
		return reviews;
	}

	public void setReviews(Set<ReviewEntity> reviews) {
		this.reviews = reviews;
	} */

    /*
    @ManyToMany
    @JoinTable(name = "review", joinColumns = @JoinColumn(name = "user_id"))
	public Set<ReviewEntity> getReviews() {
		return reviews;
	}

	public void setReviews(Set<ReviewEntity> reviews) {
		this.reviews = reviews;
	} */
 
    
}
