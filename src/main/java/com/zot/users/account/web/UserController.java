package com.zot.users.account.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.zot.Admin.category.CategoryDAO;
import com.zot.Admin.category.CategoryEntity;
import com.zot.Admin.facility.FacilityEntity;
import com.zot.Admin.review.ReviewApproval;
import com.zot.Admin.review.ReviewDAO;
import com.zot.Admin.review.ReviewEntity;
import com.zot.Admin.roomsinfo.RoomsDAO;
import com.zot.Admin.roomsinfo.RoomsEntity;
import com.zot.users.account.model.User;
import com.zot.users.account.service.SecurityService;
import com.zot.users.account.service.UserService;
import com.zot.users.account.validator.UserValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
	
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;
    
   @Autowired
   private ReviewDAO reviewDao;
   
   @Autowired
   private CategoryDAO categorydao;
   
   @Autowired
   private RoomsDAO roomsdao;
    
    public UserController(){
    	System.out.println("User controller Initiated");    	
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
    	
        model.addAttribute("userForm", new User());

        return "users/registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
    	
    	System.out.println("Indside registration POST method");
    	
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "users/registration";
        }

        userService.save(userForm);

        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:users/welcome";
    }
    
  
    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("message", "Admin Page!!");
		model.setViewName("users/admin");

		return model;

	} 

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout,@ModelAttribute("userlogin") User userLogin) {
    	
    	System.out.println("--ModelData--");
            
    	
    //	userService.findByUsername(userLogin.getUsername());
    	
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "users/login";
       // return "home";
    }
    
    
    @RequestMapping(value = "/usrlinks")
	public String navigate(HttpServletRequest request,Model model) {
     	
		String location = request.getParameter("loc");
		
		return location;
	}
    
 // for 403 access denied page
 	@RequestMapping(value = "/403", method = RequestMethod.GET)
 	public ModelAndView accesssDenied() {

 		ModelAndView model = new ModelAndView();

 		// check if user is login
 		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
 		if (!(auth instanceof AnonymousAuthenticationToken)) {
 			UserDetails userDetail = (UserDetails) auth.getPrincipal();

 			model.addObject("username", userDetail.getUsername());

 		}

 		model.setViewName("users/403");
 		return model;

 	}
    
    /*
     
    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
    
        return "users/welcome";
    } 
     
     
     */
    
 	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView welcome() {
 		
 		List<CategoryEntity> categoryList = categorydao.listCategory();
 		
 		ModelAndView model = new ModelAndView("users/welcome");
 		
 		model.addObject("categoryList", categoryList);
 		
        return model;
    }
 	
 	@RequestMapping(value = "/details", method = RequestMethod.GET)
 	public ModelAndView categorydetails(HttpServletRequest request){
 		
 		List<CategoryEntity> category;
 		
 		ModelAndView model = new ModelAndView("users/details");
 		
 		int categoryid =  Integer.parseInt(request.getParameter("catid"));
 		
 		category = categorydao.searchById(categoryid);
 		
 		model.addObject("catid", category.get(0).getCatid());
 		
 		model.addObject("categoryName", category.get(0).getCategoryName());
 		
 		model.addObject("categoryDescription", category.get(0).getDescrip());
 		
 		model.addObject("image", category.get(0).getImages());
 		
 		List<RoomsEntity> rooms = roomsdao.listRooms(categoryid);
 	   
 		model.addObject("RoomsList", rooms);
		
 		
 		return model;
 	}
    
   @RequestMapping(value="/", method = RequestMethod.GET)
    public String index() {
    	return "users/index";
    }
     
}
