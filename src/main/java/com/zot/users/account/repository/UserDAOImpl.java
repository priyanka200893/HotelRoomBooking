package com.zot.users.account.repository;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.zot.Admin.category.CategoryEntity;
import com.zot.users.account.model.Role;
import com.zot.users.account.model.User;

public class UserDAOImpl implements UserDAO {
		
	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	@Override
	public User findByUsername(String username) {
		
		 List<User> userList = new ArrayList<User>();
		 
		     System.out.println("Executing query....");
	        Query query = getSessionFactory().openSession().createQuery("from User u where u.username = :username");
	        query.setParameter("username", username);
	        userList = query.list();
	        if (userList.size() > 0)
	            return userList.get(0);
	        else
	            return null;    
		
	}
	
   
	@Override
	public void save(User user) {
		// TODO Auto-generated method stub
		
	 	Session session_1 = getSessionFactory().getCurrentSession();
		
		Transaction tx_1 = session_1.beginTransaction();
		
		session_1.persist(user);
		
		tx_1.commit();
		
		session_1.close();
		
		logger.info("User details saved successfully");
	}

	@Override
	public List<Role> findAllRoles() {
		// TODO Auto-generated method stub
		
		List<Role> list = null;
		try{
			Session session_3 = getSessionFactory().openSession();
			Transaction tx_3=session_3.beginTransaction();		
			String sql="From Role";
			Query query=session_3.createQuery(sql);
			 list =query.list();
			tx_3.commit();
			session_3.close();
		}
		catch(Exception ex){
			
			System.out.println(ex.getMessage());
			
		}
		
		return list; 
		
	}
	
}
