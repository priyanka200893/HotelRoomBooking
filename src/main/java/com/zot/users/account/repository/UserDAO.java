package com.zot.users.account.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.zot.users.account.model.Role;
import com.zot.users.account.model.User;

public interface UserDAO {
	  
	    User findByUsername(String username); 
	  
	    public void save(User user);
	    
	    public List<Role> findAllRoles();
}
