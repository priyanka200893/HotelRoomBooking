package com.zot.facilitymapping;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;

import com.zot.Admin.category.CategoryEntity;
import com.zot.Admin.facility.FacilityEntity;

@Entity
@Table(name="factilites_map_category")
public class FacilityMappingEntity {
	
	@Id
	@Column(name="id")
	@GeneratedValue
	private int id;
	
	
	@Column(name="category_id")
	private int category_id;
	
	
	@Column(name="facility_id")
	private int facility_id;


	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getFacility_id() {
		return facility_id;
	}

	public void setFacility_id(int facility_id) {
		this.facility_id = facility_id;
	}
	
}
