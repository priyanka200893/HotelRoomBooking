package com.zot.facilitymapping;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository
public class FacilityMappingDAOImp implements FacilityMappingDAO{
	@Autowired
	SessionFactory sessionFactory;
	@Override
	public void mapFacility(FacilityMappingEntity facilityMappingEntity) {		
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();		
		session.save(facilityMappingEntity);		
		tx.commit();
		session.close();		
	}
	
	
	@Override
	public List<FacilityMappingEntity> selectedFacility(int id) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();		
		String sql="From FacilityMappingEntity where category_id =:cid";
		Query query=session.createQuery(sql);
		query.setParameter("cid", id);
		List<FacilityMappingEntity> list=query.list();
		tx.commit();
		session.clear();
		return list;
		
	}


	@Override
	public void Update(FacilityMappingEntity facilityMappingEntity) {		
		
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();		
		//Query query= session.createQuery("DELETE from FacilityMappingEntity where facility_id =:facid");
		//query.setParameter("facid", fid);
		///query.executeUpdate();
		//session.delete(facilityMappingEntity);
	session.save(facilityMappingEntity);
		tx.commit();
		
		session.close();
		
		
	}


	@Override
	public void delete(int id) {
		
		Session session=sessionFactory.openSession();
		Transaction tx=session.beginTransaction();		
		Query query= session.createQuery("DELETE from FacilityMappingEntity where category_id =:facid");
		query.setParameter("facid", id);
		query.executeUpdate();
tx.commit();
		
		session.close();
	}

}
