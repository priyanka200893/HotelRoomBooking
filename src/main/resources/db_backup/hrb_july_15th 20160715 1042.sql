-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.73-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema hotelbooking
--

CREATE DATABASE IF NOT EXISTS hotelbooking;
USE hotelbooking;

--
-- Definition of table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_type` varchar(45) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `cost` varchar(200) DEFAULT NULL,
  `images` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ukey_roomtype` (`room_type`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`,`room_type`,`description`,`cost`,`images`) VALUES 
 (106,'Cat11','sadf','75.90','Jimparsons.jpg'),
 (107,'Cat12','sdf','56.60','images.jpg'),
 (108,'Cat-78','sadfsdf','79.90','images.jpg'),
 (109,'Cat-20','asdfsad','450.00','04fig04.jpg');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;


--
-- Definition of table `facilities`
--

DROP TABLE IF EXISTS `facilities`;
CREATE TABLE `facilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facilities`
--

/*!40000 ALTER TABLE `facilities` DISABLE KEYS */;
INSERT INTO `facilities` (`id`,`description`) VALUES 
 (12,'dessert'),
 (13,'Dining'),
 (22,'Wi-fi'),
 (24,'sdfsdfs'),
 (25,'fsdfsdf'),
 (26,'fsdfsdfdf'),
 (27,'fsfdgdfg'),
 (28,'gdgdgd'),
 (29,'gfddfg'),
 (30,'new facility'),
 (31,'new facility 2');
/*!40000 ALTER TABLE `facilities` ENABLE KEYS */;


--
-- Definition of table `factilites_map_category`
--

DROP TABLE IF EXISTS `factilites_map_category`;
CREATE TABLE `factilites_map_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `facility_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_PerCategory` (`category_id`),
  KEY `fk_PerFacility` (`facility_id`),
  CONSTRAINT `fk_PerCategory` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PerFacility` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `factilites_map_category`
--

/*!40000 ALTER TABLE `factilites_map_category` DISABLE KEYS */;
INSERT INTO `factilites_map_category` (`id`,`category_id`,`facility_id`) VALUES 
 (141,107,12),
 (142,107,13),
 (143,106,12),
 (144,106,13),
 (149,108,12),
 (150,108,13),
 (151,109,12),
 (152,109,13);
/*!40000 ALTER TABLE `factilites_map_category` ENABLE KEYS */;


--
-- Definition of table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation` (
  `res_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `check_in_date` datetime NOT NULL,
  `check_out_date` datetime NOT NULL,
  `check_in_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `check_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `other_info` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`res_id`),
  KEY `fk_uid` (`user_id`),
  KEY `fk_roomid` (`room_id`),
  CONSTRAINT `fk_roomid` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`),
  CONSTRAINT `fk_uid` FOREIGN KEY (`user_id`) REFERENCES `userdetails` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;


--
-- Definition of table `reservation_approval`
--

DROP TABLE IF EXISTS `reservation_approval`;
CREATE TABLE `reservation_approval` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `res_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `approved_status` varchar(45) NOT NULL,
  `Message` varchar(500) NOT NULL,
  `approved_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_res_id` (`res_id`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `fk_res_id` FOREIGN KEY (`res_id`) REFERENCES `reservation` (`res_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `userdetails` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation_approval`
--

/*!40000 ALTER TABLE `reservation_approval` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_approval` ENABLE KEYS */;


--
-- Definition of table `review`
--

DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `rev_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `review` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`rev_id`),
  KEY `fk_user_idd` (`user_id`),
  CONSTRAINT `fk_user_idd` FOREIGN KEY (`user_id`) REFERENCES `userdetails` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` (`rev_id`,`user_id`,`review`) VALUES 
 (2,1,'yyyy'),
 (3,1,'sadfsad'),
 (4,1,'New Review Added'),
 (5,1,'Just now one review Review added');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;


--
-- Definition of table `review_approval`
--

DROP TABLE IF EXISTS `review_approval`;
CREATE TABLE `review_approval` (
  `rev_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`rev_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review_approval`
--

/*!40000 ALTER TABLE `review_approval` DISABLE KEYS */;
/*!40000 ALTER TABLE `review_approval` ENABLE KEYS */;


--
-- Definition of table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`,`name`) VALUES 
 (1,'ROLE_USER'),
 (2,'ROLE_ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


--
-- Definition of table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `room_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(10) unsigned NOT NULL,
  `no_of_rooms` int(10) unsigned NOT NULL,
  PRIMARY KEY (`room_id`),
  KEY `fk_category` (`id`),
  CONSTRAINT `fk_category` FOREIGN KEY (`id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`room_id`,`id`,`no_of_rooms`) VALUES 
 (6,107,49),
 (7,109,45);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;


--
-- Definition of table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`,`username`,`password`,`email`) VALUES 
 (4,'madhu11','$2a$11$cWWKkmdXxD12C4HIXL.vy.nf9UWt9AHmiBSSOQytPnxYFN936gfEq',NULL),
 (5,'vijaykrishna','$2a$11$ZH1xYlHeLx3aOHYoIjVP2.6W2Aq7FHypgUHEGfpmFd4V2HnE0dRPm',NULL),
 (6,'Admin_1','$2a$11$cWWKkmdXxD12C4HIXL.vy.nf9UWt9AHmiBSSOQytPnxYFN936gfEq',NULL),
 (7,'Admin_2','$2a$11$cWWKkmdXxD12C4HIXL.vy.nf9UWt9AHmiBSSOQytPnxYFN936gfEq',NULL),
 (8,'Krishna','$2a$11$uXN/o3JPUt7j1L09vyX./efpakk1vngLsU.gLQRNsvRprS24nyg4W',NULL),
 (9,'Madhuk123','$2a$11$DFgB0KY6DWxSoumNfbJroeucLq.SFwMrWFUUVzjqtg5YcGTjCJVaS',NULL),
 (10,'Madhuk124','$2a$11$pqRmjfGKVi5PdwbkP5YiMur/zOyR.W8zNqt1YK6JKNjNDzcm47PUS',NULL),
 (11,'user55','$2a$11$DFgB0KY6DWxSoumNfbJroeucLq.SFwMrWFUUVzjqtg5YcGTjCJVaS',NULL),
 (13,'user56','$2a$11$cWWKkmdXxD12C4HIXL.vy.nf9UWt9AHmiBSSOQytPnxYFN936gfEq',NULL),
 (14,'user59','$2a$11$hPvR25394J1yH257AbCGqOWPnlXxH8W9/RsFb6sT58b94p.XwbBYS',NULL),
 (15,'User60','$2a$11$OiFYuOUNDfumbSWhTkU.Cesk7pLEwXPEVaW8FW0erD0Do9d7lX0Ny','madhurrsc@gmail.com'),
 (16,'User61','$2a$11$HALqt/Ubrdoue/fF8fsXwu1X5HnirGIavZC8twHfJ33pAPh8tbyRu','madhuknew@gmail.com');
INSERT INTO `user` (`id`,`username`,`password`,`email`) VALUES 
 (17,'madhurrsc','$2a$11$rCM1RnHDedPtZ1RJ2dCZBu3p16gS6FF3Fve2mhxCSurnRc2bkmOwu','madhurrsc@gmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


--
-- Definition of table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_user_role_roleid_idx` (`role_id`),
  CONSTRAINT `fk_user_role_roleid` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_role_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`user_id`,`role_id`) VALUES 
 (4,1),
 (5,1),
 (6,1),
 (7,1),
 (8,1),
 (9,1),
 (10,1),
 (11,1),
 (13,1),
 (14,1),
 (15,1),
 (16,1),
 (17,1),
 (6,2),
 (7,2),
 (8,2),
 (10,2),
 (11,2),
 (14,2),
 (15,2),
 (16,2),
 (17,2);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;


--
-- Definition of table `userdetails`
--

DROP TABLE IF EXISTS `userdetails`;
CREATE TABLE `userdetails` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `contactno` varchar(50) NOT NULL,
  `address` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdetails`
--

/*!40000 ALTER TABLE `userdetails` DISABLE KEYS */;
INSERT INTO `userdetails` (`user_id`,`first_name`,`last_name`,`contactno`,`address`) VALUES 
 (1,'priyanka','rajkumar','9677103630','palavakkam');
/*!40000 ALTER TABLE `userdetails` ENABLE KEYS */;


--
-- Definition of table `users_map`
--

DROP TABLE IF EXISTS `users_map`;
CREATE TABLE `users_map` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `user_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_PerUserType` (`user_type_id`),
  KEY `fk_peruseridd` (`user_id`),
  CONSTRAINT `fk_PerUserId` FOREIGN KEY (`id`) REFERENCES `userdetails` (`user_id`),
  CONSTRAINT `fk_peruseridd` FOREIGN KEY (`user_id`) REFERENCES `userdetails` (`user_id`),
  CONSTRAINT `fk_PerUserType` FOREIGN KEY (`user_type_id`) REFERENCES `usertypes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_map`
--

/*!40000 ALTER TABLE `users_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_map` ENABLE KEYS */;


--
-- Definition of table `usertypes`
--

DROP TABLE IF EXISTS `usertypes`;
CREATE TABLE `usertypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usertype` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usertypes`
--

/*!40000 ALTER TABLE `usertypes` DISABLE KEYS */;
INSERT INTO `usertypes` (`id`,`usertype`) VALUES 
 (1,'Super user'),
 (2,'Admin user'),
 (3,'Guest user'),
 (4,'Registered user');
/*!40000 ALTER TABLE `usertypes` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
