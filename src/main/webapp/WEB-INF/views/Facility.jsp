<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>

<div>
<p>
<span style="float:right">
	   <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome ${pageContext.request.userPrincipal.name} | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>

  </c:if>
</span>
</p>  
</div>


<form name="facilityform" method="POST" action="${contextPath}/admin/addfacility?${_csrf.parameterName}=${_csrf.token}">
<table>
<tr>
<td>
<%@include file="/WEB-INF/views/LogoPage.jsp" %>
</td>
</tr>
</table>
<hr>
<table align="center">
<tr>
<th align="left" class="linkpos"><%@include file="/WEB-INF/views/Links.jsp" %></th>
<td><table align="center">
<caption><b>Add Facility</b></caption>
<tr>
<th> Name of the Facility </th>
<td><input type="text" name="facname"/></td>
<td>${msg}</td>
<td>${facname }</td>
</tr>
<tr>
<td>
<!-- <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> -->
<input type="submit" value="Add Facility">
</td>
</tr>
<tr>

</tr>
</table></td>
</tr>
</table>

</form>
</body>
</html>