<%@page import="com.zot.facilitymapping.FacilityMappingEntity"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.zot.Admin.category.*" %>
<%@page import="com.zot.Admin.facility.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>
<table>
<tr>
<td>
<%@include file="/WEB-INF/views/LogoPage.jsp" %>
</td>
</tr>

</table>
<hr>
<table align="center">
<tr>
<th class="linkpos"><%@include file="/WEB-INF/views/Links.jsp" %></th>
<td><form  name="editCategoryForm" action="${contextPath}/admin/Updatecategory?${_csrf.parameterName}=${_csrf.token}" method="POST"  enctype="multipart/form-data">
<% 

ArrayList<CategoryEntity> list=( ArrayList<CategoryEntity>)request.getAttribute("list"); 
for(int i=0;i<list.size();i++)
{
%>
<table align="center">

<caption>Edit Category</caption>
<tr>
<td><input type="hidden" name="catid" value="<% out.print(list.get(i).getCatid()); %>"></td>

</tr>
<tr>
<td>Category Name</td>
<td><input type="text"  name="categoryName" value="<%out.print(list.get(i).getCategoryName());  %>"  /></td>
<td><input type="hidden"  name="checkname" value="<%out.print(list.get(i).getCategoryName());  %>"  /></td>
<td>${catname}</td> 
<td>${msg}</td>
</tr>
<tr>
<td>Cost</td>
<td><input type="text"  name="Cost" value="<%out.print(list.get(i).getCost()); %>" /></td>
<td>${cost}</td>
<td>${costexp }</td>
</tr>
<tr>
<td>Description</td>
<td><textarea id="addtext" name="descrip" rows="5" cols="20"><%out.print(list.get(i).getDescrip()); %></textarea></td>
<td>${desc}</td>
</tr>

<tr>
<th>Facilities </th>
<td  class="tdchk"> 
<table>
<tr>

<% if(request.getAttribute("list1")!=null){
	ArrayList<FacilityEntity> list1=(ArrayList<FacilityEntity>)request.getAttribute("list1");	
	ArrayList<FacilityMappingEntity> map=(ArrayList<FacilityMappingEntity>)request.getAttribute("selected");
	int row=0;	
	for(int i1=0;i1<list1.size();i1++)
{
	
	if (row==3)
	{
		row=0;
		%></tr> <% 
	}
	else {			
	%>  
<td><input type="checkbox" name="chk"  value=<% out.print(list1.get(i1).getFacid()); %> 
<%
 for( int j=0;j<map.size();j++ )	
{
	
	if (list1.get(i1).getFacid()==map.get(j).getFacility_id())
	{
		%>
		checked="checked" 
		<%
	}
}
%>
><% out.print(list1.get(i1).getFacname());%> </input></td> 
<% 
row++;
}
}
}
%>
</table>
 </td>
<td>${chk}</td>
</tr>

<tr>
<th>Upload Image</th>
<td><img src="resources/uploads/<%out.print(list.get(i).getCategoryName()+list.get(i).getImages()); %>" class="circle"/> </td>
<td><input type="file" name="images"  value="<%out.print(list.get(i).getImages()); %>" ></input> 
<td><input type="hidden" readonly="readonly"  name="upload"   value="resources/uploads/<%out.print(list.get(i).getImages());  %>" /></td>
<td>${img }</td>
<td>

</td>
</tr>
<tr>
<td><input type="submit" name="btn" value="Update catgeory"></td>
</tr>

</table>
</form>
<%} %></td>
</tr>
</table>

</body>
</html>