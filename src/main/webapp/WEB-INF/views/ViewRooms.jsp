<%@page import="com.zot.Admin.roomsinfo.RoomsEntity"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>

<% ArrayList<RoomsEntity> list=( ArrayList<RoomsEntity>)request.getAttribute("getroomslist"); %>
<table>
<tr>
<td>
<%@include file="/WEB-INF/views/LogoPage.jsp" %>
</td>
</tr>
</table>
<hr>
<table align="center">
<tr>
<th class="linkpos"><%@include file="/WEB-INF/views/Links.jsp" %></th>
<td> <table align="center"> <tr>
<td class="linkpos"></td>
<td><table>
<tr>
<th>Category Name</th>
<th>Number Of Rooms</th>
<th>Edit</th>
<th>Delete</th>
</tr>
<% for(int i=0;i<list.size();i++) { 
	%> 
	<tr>
<td><% out.print( list.get(i).getCategoryEntity().getCategoryName()); %></td>
<td><% out.print(list.get(i).getRoomNos()); %></td>
<th><a href="EditRooms?rid=<% out.print(list.get(i).getCategoryEntity().getCatid()); %>">Edit</a></th>
<th><a href="DeleteRooms?rid=<% out.print(list.get(i).getCategoryEntity().getCatid()); %>">Delete</a></th>
</tr>
	<%
	
} %>
</table> </td>
</tr>
 </table>
</td>
</tr>
</table>

</body>
</html>