<%@page import="com.zot.Admin.category.CategoryEntity"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.zot.Admin.category.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>
<table>
<tr>
<td><%@include file="/WEB-INF/views/LogoPage.jsp" %></td>
</tr>
</table>
<hr/>
<table align="center">
<tr>
<th class="linkpos"><%@include file="/WEB-INF/views/Links.jsp" %> </th>
<td><form name="view" method="post" > 
<table >
<tr>
<th>Category Name</th>
<th>Cost</th>
<th>Description</th>
<th>Edit</th>
<th>Delete</th>
</tr>
<% 
if(request.getAttribute("listCategory")!=null){
ArrayList<CategoryEntity> list= (ArrayList<CategoryEntity>)request.getAttribute("listCategory");
for(int i=0;i<list.size();i++)
{
	%>
<tr>
<td>
<% 	out.print(list.get(i).getCategoryName()); %>
</td>
<td><%  out.print(list.get(i).getCost()); %> </td>
<td><%  out.print(list.get(i).getDescrip()); %> </td>
<td><a href="Edit?id=<% out.print(list.get(i).getCatid()); %>"  >Edit</a></td>
<td><a href="Delete?id=<% out.print(list.get(i).getCatid()); %>"  >Delete</a></td>
</tr>
<% }
}%>
</table>
</form><br>
 </td>
</tr>
</table>

</body>
</html>