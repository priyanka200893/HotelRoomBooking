<%@page import="java.util.ArrayList"%>
<%@page import="com.zot.Admin.category.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>  
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>
<table>
<tr>
<td>
<%@include file="/WEB-INF/views/LogoPage.jsp" %>
</td>
</tr>
</table>
<hr>
<table align="center">
<tr>
<td class="linkpos"><%@include file="/WEB-INF/views/Links.jsp" %></td>
<td>
<form name="addrooms" action="${contextPath}/admin/addrooms?${_csrf.parameterName}=${_csrf.token}" method="post">
<table>
<caption>Add Rooms</caption>
<tr>
<th>Choose the category</th>
<td><select  name="catid"  >
<option value="default" >--Choose one category--</option>
<% ArrayList<CategoryEntity> list=(ArrayList<CategoryEntity>)request.getAttribute("roomlist"); 
for(int i=0;i<list.size();i++)
{
	%> 
	<option value="<% out.print(list.get(i).getCatid());%>"><%out.print( list.get(i).getCategoryName());  %></option>
	<%
	}
%>
</select></td>
<td>${idnull}</td>
<td>${catdef}</td>
</tr>
<tr>
<th>Enter the number of rooms</th>
<td><input type="text" name="roomNos" value="${rooms.roomNos}"/></td>
<td>${num}</td>
<td>${num1}</td>
</tr>
<tr>

</tr>
<tr>
<td colspan="2" align="center"><input type="submit" name="subbtn" value="Add rooms"/></td>
<td></td>
</tr>
<tr>
<th>${msg}</th>
</tr>
</table>
</form>
</td>
</tr>
</table>


</body>
</html>