<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>The Paradise-Hotel Website Template | Hotel :: w3layouts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
<link href="${contextPath}/resources/web/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">

<script src="${contextPath}/resources/web/js/jquery.min.js"></script>
<!--start slider -->
<link rel="stylesheet" href="${contextPath}/resources/web/css/fwslider.css" media="all">
<script src="${contextPath}/resources/web/js/jquery-ui.min.js"></script>
<script src="${contextPath}/resources/web/js/css3-mediaqueries.js"></script>
<script src="${contextPath}/resources/web/js/fwslider.js"></script>
<!--end slider -->
<!---strat-date-piker---->
<link rel="stylesheet" href="${contextPath}/resources/web/css/jquery-ui.css" />
<script src="${contextPath}/resources/web/js/jquery-ui.js"></script>
		  <script>
				  $(function() {
				    $( "#datepicker,#datepicker1" ).datepicker();
				  });
		  </script>
<!---/End-date-piker---->
<link type="text/css" rel="stylesheet" href="${contextPath}/resources/web/css/JFGrid.css" />
<link type="text/css" rel="stylesheet" href="${contextPath}/resources/web/css/JFFormStyle-1.css" />
		<script type="text/javascript" src="${contextPath}/resources/web/js/JFCore.js"></script>
		<script type="text/javascript" src="${contextPath}/resources/web/js/JFForms.js"></script>
		<!-- Set here the key for your domain in order to hide the watermark on the web server -->
		<script type="text/javascript">
			(function() {
				JC.init({
					domainKey: ''
				});
				})();
		</script>
<!--nav-->
<style>
	a {
	cursor: pointer;
}
	
</style>

<script>
		$(function() {
			var pull 		= $('#pull');
				menu 		= $('nav ul');
				menuHeight	= menu.height();

			$(pull).on('click', function(e) {
				e.preventDefault();
				menu.slideToggle();
			});

			$(window).resize(function(){
        		var w = $(window).width();
        		if(w > 320 && menu.is(':hidden')) {
        			menu.removeAttr('style');
        		}
    		});
		});
</script>
</head>
<body>
<!-- start header -->
<div class="header_bg">
<div class="wrap">
	<div class="header">
		<div class="logo">
			<a href="${contextPath}/index"><img src="${contextPath}/resources/web/images/logo.png" alt=""></a>
		</div>
		<div class="h_right">
			<!--start menu -->
			<ul class="menu">
				<li class="active"><a href="${contextPath}/index">hotel</a></li> |
				<li><a href="${contextPath}/resources/web/rooms.html">rooms & suits</a></li> |
				<li><a href="${contextPath}/resources/web/reservation.html">reservation</a></li> |
			<!-- <li><a href="${contextPath}/resources/web/activities.html">activities</a></li> | 
			     <li><a href="${contextPath}/login">Login</a></li>  | --> 
				<li><a href="${contextPath}/resources/web/contact.html">contact</a></li>
				<div class="clear"></div>
			</ul>
			<!-- start profile_details -->
					<form class="style-1 drp_dwn">
						<div class="row">
							<div class="grid_3 columns">
								<select class="custom-select" id="select-1">
									<option selected="selected">EN</option>
									<option>USA</option>
									<option>AUS</option>
									<option>UK</option>
									<option>IND</option>
								</select>
							</div>		
						</div>		
					</form>
		</div>
		<div class="clear"></div>
		<div class="top-nav">
		<nav class="clearfix">
				<ul>
				<li class="active"><a href="${contextPath}/index">hotel</a></li> 
				<li><a href="${contextPath}/resources/web/rooms.html">rooms & suits</a></li> 
				<li><a href="${contextPath}/resources/web/reservation.html">reservation</a></li> 
				<li><a href="${contextPath}/resources/web/activities.html">activities</a></li> 
				<li><a href="${contextPath}/resources/web/contact.html">contact</a></li>
				</ul>
				<a href="#" id="pull">Menu</a>
			</nav>
		</div>
	</div>
</div>
</div>