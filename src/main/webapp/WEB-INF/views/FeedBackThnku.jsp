<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Thank you</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>
<table>
<tr>
<td>
<%@include file="/WEB-INF/views/LogoPage.jsp" %>
</td>
</tr>
</table>
<hr>
<table align="center">
<tr>
<td class="linkpos"><%@include file="/WEB-INF/views/UserLinks.jsp" %></td>
<td>
<table>
<tr>
<th>${success}</th>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>