<%@page import="com.zot.Admin.review.ReviewEntity"%>
<%@page import="java.util.ArrayList"%>
<%@page  import=" java.text.DateFormat" %>
<%@page  import=" java.text.SimpleDateFormat"%>
<%@page  import=" java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.zot.Admin.review.*" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Edit Approval</title>
		<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
	</head>
	<body>
	
	<table>
		<tr><td><%@include file="/WEB-INF/views/LogoPage.jsp" %></td></tr>
	</table>
	<hr/>
	<table align="center">
	<tr>
	<form name="frmRequest" action="${contextPath}/searchStatus?${_csrf.parameterName}=${_csrf.token}" method="POST">
	<th align="center">Choose The State</th>
	<td align="center">
	<select name="statusrequest" >
	<option value="1">--Select-- </option>
		<option value="all">All</option>
		<option value="Approved">Approve</option>
		<option value="Reject">Reject</option>
		<option value="Pending" selected="selected">Pending</option>
	 </select>
	 <input type="submit" value="search">
	</td>
	<td>${statuserr}</td>
	</form>
	</tr>
	<div style="visibility:<%out.print(request.getAttribute("style1"));%>;" >
	<tr>
	<th class="linkpos"><%@include file="/WEB-INF/views/UserLinks.jsp" %> </th>
	<td >
		<table>
		<tr >
		<th>User name</th>
		<th>Feed Back</th>
		<th>Status</th>
		<th>Suggestion</th>
		<th>Action</th>
		</tr>
		<% 
	if(request.getAttribute("reviewslist")!=null){
	ArrayList<ReviewApproval> reviewList =
	(ArrayList<ReviewApproval>)request.getAttribute("reviewslist");
	%>
	<% for(int i=0;i<reviewList.size();i++)  
		{
		%>
		<tr>
		<!-- <form name="frmrev"  action="${contextPath}/updateStatus?${_csrf.parameterName}=${_csrf.token}" method="POST"> -->
		<form name="frmrev"  action="${contextPath}/admin/updateStatus?${_csrf.parameterName}=${_csrf.token}" method="POST">
		<td> <input type="hidden"  name="user_id" value="<%out.print(reviewList.get(i).getUser_id()); %>" /> <%out.print( reviewList.get(i).getUser_id()); %></td>
	<td><%out.print( reviewList.get(i).getReviewEntity().getFeedback()); %></td>
		<td>
		<select name="status" >
		<option value="Approved">Approve</option>
		<option value="Reject">Reject</option>
		<option value="Pending" >Pending</option>
		 </select> 
		 </td>
		 <td><textarea rows="5" cols="20" name="message"><%out.print(reviewList.get(i).getMessage()); %></textarea>  </td>
		 <td><input type="submit" value="Update FeedBack"  name="btnUpdate"/></td>
		  <td><input type="submit" value="Delete FeedBack" name="btnDelete" /></td>
		 <td> <input type="hidden" name="app_id" value="<%out.print(reviewList.get(i).getApp_id());%>" /></td>
		<td style="visibility: hidden;"><input type="text" name="id" value="<%out.print(reviewList.get(i).getReviewEntity().getId()); %>" /> <%out.print( reviewList.get(i).getReviewEntity().getId()); %></td>
		<td><input type="hidden" name="editeddate" value="<%out.print(reviewList.get(i).getEditeddate()); %>"/></td>
		<td><input type="hidden" name="editedtime" value="<%out.print(reviewList.get(i).getEditedtime()); %>"/></td>		 
	</form>
	</tr>
		
	<% 	}
	}		
	%>
		<tr>		
		</tr>		
		</table>
	</td>
	</tr>
	</div>
	<tr><th>${records}</th></del></tr>
	</table>
	</body>
</html>