<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>
<table>
<tr>
<td>
<%@include file="/WEB-INF/views/LogoPage.jsp" %>
</td>
</tr>
</table>
<hr>
<table align="center">
<tr>
<td class="linkpos"><%@include file="/WEB-INF/views/UserLinks.jsp" %></td>
<td>
<form name="frm" action="${contextPath}/validate?${_csrf.parameterName}=${_csrf.token}" method="POST">
<table>
<tr>
<th>User id</th>
<th><input type="text" name="user_id" value="1" readonly="readonly"/></th>
</tr>
<tr>
<th>Add FeedBack</th>
<td><textarea rows="10" cols="20" name="feedback"></textarea></td>
<td>${rev}</td>
</tr>
<tr style="visibility: hidden;"><th>Status</th>
<td><input type="text" name="status" value="Pending" readonly="readonly"></td>
</tr>
<tr>
<td colspan="2" align="center"><input type="submit" value="Add Review" name="btnsub"/></td>
<td></td>
</tr>
</table>
</form>
</td>
</tr>
</table>

</body>
</html>