<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.zot.Admin.review.*"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Approval</title>
</head>
<body>
<% ArrayList<ReviewApproval> reviewList =(ArrayList<ReviewApproval>)request.getAttribute("reviewslist");

%>
<table>
<tr>
<td><%@include file="/WEB-INF/views/LogoPage.jsp" %></td>
</tr>
</table>
<hr/>
<table align="center">
<tr>
<th class="linkpos"><%@include file="/WEB-INF/views/UserLinks.jsp" %> </th>
<td>
<form name="frmrev"  action="${contextPath}/updateStatus?${_csrf.parameterName}=${_csrf.token}" method="POST">
<table >
<tr>
<th>User name</th>
<th>Feed Back</th>
<th>Status</th>
<th>Suggestion</th>
<th>Action</th>
</tr>
<% for(int i=0;i<reviewList.size();i++)  {
%>
<tr>
 <td><input type="hidden"  name="user_id" value="<%out.print(reviewList.get(i).getUser_id()); %>" /> <%out.print( reviewList.get(i).getUser_id()); %></td>
<td><%out.print( reviewList.get(i).getReviewEntity().getFeedback()); %></td>
<td>
<select name="status" >
<option value="1">--Select--</option>
<option value="Approved">Approve</option>
<option value="Reject">Reject</option>
<option value="Pending" selected="selected">Pending</option>
 </select>
 </td>
 
 
 <td><textarea rows="5" cols="20"></textarea>  </td>
 <td><input type="submit" value="Update status"/></td>
 <td style="visibility: hidden;"><input type="text" name="editeddate" value="nothing" /></td>
 <td> <input type="hidden" name="app_id" value="<%out.print(reviewList.get(i).getApp_id());%>"></td>
<td style="visibility: hidden;" ><input type="text" name="id" value="<%out.print(reviewList.get(i).getReviewEntity().getId()); %>" /> <%out.print( reviewList.get(i).getReviewEntity().getId()); %></td>
</tr>
<%	
}%>

</table>
</form>
</td>
</tr>
</table>
</body>
</html>