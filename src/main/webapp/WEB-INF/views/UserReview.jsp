<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.zot.Admin.review.*"%>
      <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>user reviews</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>
<table>
		<tr><td><%@include file="/WEB-INF/views/LogoPage.jsp" %></td></tr>
	</table>
	<hr/>
	<table align="center">
	<tr>
<th><th class="linkpos"><%@include file="/WEB-INF/views/UserLinks.jsp" %></th>
<td>
<% 
if(request.getAttribute("userReviewList")!=null) {
ArrayList<ReviewApproval> userReviewlist=( ArrayList<ReviewApproval>)
request.getAttribute("userReviewList"); 

%>
<%  for(int i=0;i<userReviewlist.size();i++) { %>
<form name="frmUserReviews" action="${contextPath}/userviews?${_csrf.parameterName}=${_csrf.token}" method="POST">
<table>
	<tr>
	<td style="visibility: hidden;">User Id</td>
	<th>ReviewId</th>
	<th>FeedBack</th>
	<th>Status</th>
	<th>Message By Administrator</th>
	<th>Update</th>
	<th>Delete</th>
	</tr>
	<tr>
	<td><input type="hidden" value="<%out.print(userReviewlist.get(i).getUser_id()); %>" name="user_id"  readonly="readonly" /></td>
	<td><input type="hidden"  name="id"  value="<%out.print(userReviewlist.get(i).getReviewEntity().getId()); %>"/>
	<% out.print(userReviewlist.get(i).getReviewEntity().getId());%></td>
	<td><textarea name="feedback" rows="5" cols="20"><%out.print(userReviewlist.get(i).getReviewEntity().getFeedback()); %>
	</textarea>  ${feedback }</td>
	
	<td><%out.print(userReviewlist.get(i).getStatus()); %></td>
	<td><%out.print(userReviewlist.get(i).getMessage()); %></td>
	<td><input type="submit" value="Update" name="UpdateFeedback" /></td>
	<td><input type="submit" value="Delete" name="DeleteFeedback" /></td>
	</tr>

</table>
</form>
<% } }%>
</td>
</tr>
<tr>
<th>${records }</th>
</tr>
</table>
</body>
</html>