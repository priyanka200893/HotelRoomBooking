<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User links</title>
</head>
<body>
<table>
<tr>
<th><a href="usrlinks?loc=AddReview">Add Review</a></th>
</tr>
<tr>
<th><a href="usrlinks?loc=ReviewDispatch">Approve/Reject Review</a></th>
</tr>
<tr>
<th><a href="usrlinks?loc=ViewReviewsDispatch">Users View Reviews</a></th>
</tr>
</table>
</body>
</html>