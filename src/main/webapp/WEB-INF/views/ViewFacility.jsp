<%@page import="com.zot.Admin.facility.FacilityEntity"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.zot.Admin.facility.*" %>>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>
<% ArrayList<FacilityEntity> list=(ArrayList<FacilityEntity>)request.getAttribute("list"); %>
<table>
<tr>
<td>
<%@include file="/WEB-INF/views/LogoPage.jsp" %>
</td>
</tr>
</table>
<hr>
<table align="center">
<tr>
<td align="left" class="linkpos"><%@include file="/WEB-INF/views/Links.jsp" %></td>
<td align="center"><table align="right" >
<caption>List of facility</caption>
<tr>
<th>Facility id</th>
<th>Facility name</th>
<td>Edit</td>
<th>Delete</th>
</tr>
<% for(int i=0;i<list.size();i++){  %>
<tr>
<td><% out.print(list.get(i).getFacid()); %></td>
<td><% out.print(list.get(i).getFacname());%></td>
<td><a href="edit?fid=<%out.print(list.get(i).getFacid()); %>">Edit</a></td>
<td><a href="delete?fid=<%out.print(list.get(i).getFacid()); %>">Delete</a></td>
</tr>
<% } %>
</table></td>
</tr>
</table>

</body>
</html>