<%@page import="com.zot.Admin.facility.FacilityEntity"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.awt.Checkbox"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@include file="/WEB-INF/views/header.jsp" %>

<div>
<p>
<span style="float:right">
	   <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome ${pageContext.request.userPrincipal.name} | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>

  </c:if>
</span>
</p>  
</div>


<table>
<tr>
<td>
<%@include file="/WEB-INF/views/LogoPage.jsp" %>
</td>
</tr>
</table >
<hr>

<table align="center" >
<tr>
<th class="linkpos"><%@include file="/WEB-INF/views/Links.jsp" %></th>
<td> <table align="center" border="1">
<caption><font size="4"><b>CATEGORY DETAILS</b></font></caption>
<tr>
<th align="right">
CategoryName
</th>
<td align="center">${category.categoryName }</td>
</tr>
<tr>
<th align="right">
Description
</th>
<td align="center">
${category.descrip}

</td>
</tr>
<tr>
<th align="right">
Cost
</th>
<td align="center">
${category.cost}
</td>
</tr>
<tr>
<th align="right">Facilities</th>
<td align="center">
<% 
String[] facility=request.getParameterValues("chk");
ArrayList<FacilityEntity> list=(ArrayList<FacilityEntity>)request.getAttribute("list");
out.print("<table>");
for(int i=0;i<facility.length;i++)
{
for(int j=0;j<list.size();j++)
{
	if(facility[i].equals(Integer.toString((list.get(j).getFacid()))))
	{
		out.print("<tr><td><b>--</b>"+list.get(j).getFacname()+"</td></tr>");
	}
	}
}

out.print("</table>");

%>
</td>
</tr>
<tr>
<th align="right">
Image
</th>
<td align="left">
<img height="250px" width="250px"  src="resources/uploads/${category.categoryName}${category.images}" />

</td>
</tr>
<tr>
<th><th>${success}</th></th>
</tr>
</table> </td>
</tr>
</table>
<%@include file="/WEB-INF/views/footer.jsp" %>
</body>
</html>