<%@page import="org.springframework.validation.BindingResultUtils"%>
<%@page import="org.springframework.validation.BindingResult"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.zot.Admin.facility.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
    
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Add category</title>


<!--  <link rel="stylesheet" type="text/css" href="overall.css"/> -->
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" /> 
<script type="text/javascript" language="javascript">
function check()
{
	var phonepat = "/^\d{10}$/";
	var cost=document.categoryform.Cost.value; 
	var cnt=document.categoryform.Cost.value;
	alert(cnt);
	if(!cnt.match(phonepat))
		{ 
		
		alert("Enter only numbers");
		document.categoryform.Cost.value.focus();
		return false;
		}
	return true;
	
	}
</script>
</head>
<body>
<table>
<tr>
<td  >
<%@include file="/WEB-INF/views/LogoPage.jsp" %>
</td>
</tr>
</table>
<hr>
<form name="categoryform" method="POST" action="${contextPath}/admin/addcategory?${_csrf.parameterName}=${_csrf.token}"  enctype="multipart/form-data">

<table align="center">
<caption><b>Add categories</b></caption>
<tr>
<td  class="linkpos"><%@include file="/WEB-INF/views/Links.jsp" %> </td>
<td>
<table> 
<tr>
<th > Category Name </th>
<td><input type="text" name="categoryName" value="${category.categoryName }"/></td>
<td>${catname }</td>
<td>${msg }</td>
</tr>
<tr>
<th>cost</th>
<td><input type="text" name="Cost" value="${category.cost}" /></td>

<td>${cost }</td>
</tr>

<tr>
<th>Description</th>
<td><textarea id="addtext" name="descrip" rows="5" cols="20">${category.descrip}</textarea></td>
<td>${desc}</td>
</tr>
<tr>
<th>Facilities </th>
	<td  class="tdchk"> 
<table>
<tr>
<% if(request.getAttribute("list")!=null)
{
	ArrayList<FacilityEntity> list=(ArrayList<FacilityEntity>)request.getAttribute("list");
int row=0;
for(int i=0;i<list.size();i++)
{	
if(row==3)
{
	row=0;
	%></tr><%
}
else{ %>
<td><input type="checkbox" name="chk" value=<% out.print(list.get(i).getFacid()); %> >  <% out.print(list.get(i).getFacname()); %></input</td>
 
<% 
row++;
}
}
}
%>
</table>
</td>
<td>${map }</td>


</tr>

<tr>
<th>Upload Image</th>
<td><input type="file" name="images"  /> 
<td>${img }</td>
</tr>

<tr>
<td>
<!-- <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> -->
<input type="submit" value="Add category" >
</td>
</tr>
<tr>
</tr>
</table>
</td>
</tr>
<tr>
<th>${success}</th>
</tr>


</table>
</form>

</body>
</html>