<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Review Page</title>
</head>
<body>

<%@include file="/WEB-INF/views/header.jsp" %>
<%@include file="/WEB-INF/views/logout.jsp" %>

<table align="center">
<tr>
<td></td>
<td>
<form name="frm" action="${contextPath}/usrlinks/validate?${_csrf.parameterName}=${_csrf.token}" method="POST">
<table>
<tr>
<th>User id</th>
<th><input type="text" name="user_id" value="1" readonly="readonly"/></th>
</tr>
<tr>
<th>Add FeedBack</th>
<td><textarea rows="10" cols="20" name="feedback"></textarea></td>
<td>${rev}</td>
</tr>
<tr style="visibility: hidden;"><th>Status</th>
<td><input type="text" name="status" value="Pending" readonly="readonly"></td>
</tr>
<tr>
<td colspan="2" align="center"><input type="submit" value="Add Review" name="btnsub"/></td>
<td></td>
</tr>
</table>
</form>
</td>
</tr>
</table>
<%@include file="/WEB-INF/views/footer.jsp" %>
</body>
</html>