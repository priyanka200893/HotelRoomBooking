<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<%@include file="/WEB-INF/views/header.jsp" %>
<div id="container">
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        
       <h2>Welcome ${pageContext.request.userPrincipal.name}</h2>
        <p>
           <span style="margin-left:20px">| <a href="${contextPath}/user/bookedhistory">View Booked History</a>
           </span>
           <span style="margin-left:10px">| <a href="${contextPath}/user/editaccount">Edit Account</a>
           </span>
           <span style="float:right">| <a onclick="document.forms['logoutForm'].submit()">Logout</a>
           </span>
        </p>
    </c:if>
    
</div>

<!--start main -->
<div class="main_bg">
<div class="wrap">
	<br><br><br><br>
	<div class="online_reservation">
	<div class="b_room">
		<div class="booking_room">
			<h4>book a room online</h4>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
		</div>
		<div class="reservation">
			<ul>
				<li class="span1_of_1">
					<h5>type of room:</h5>
					<!----------start section_room----------->
					<div class="section_room">
						<select id="country" onchange="change_country(this.value)" class="frm-field required">
							<option value="null">Select a type of room</option>
				            <option value="null">Suite room</option>         
				            <option value="AX">Single room</option>
							<option value="AX">Double room</option>
		        		</select>
					</div>	
				</li>
				<li  class="span1_of_1 left">
					<h5>check-in-date:</h5>
					<div class="book_date">
						<form>
							<input class="date" id="datepicker" type="text" value="DD/MM/YY" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'DD/MM/YY';}">
						</form>

					</div>					
				</li>
				<li  class="span1_of_1 left">
					<h5>check-out-date:</h5>
					<div class="book_date">
						<form>
							<input class="date" id="datepicker1" type="text" value="DD/MM/YY" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'DD/MM/YY';}">
						</form>
					</div>		
				</li>
				<li class="span1_of_2 left">
					<h5>Adults:</h5>
					<!----------start section_room----------->
					<div class="section_room">
						<select id="country" onchange="change_country(this.value)" class="frm-field required">
							<option value="null">1</option>
				            <option value="null">2</option>         
				            <option value="AX">3</option>
							<option value="AX">4</option>
		        		</select>
					</div>					
				</li>
				<li class="span1_of_3">
					<div class="date_btn">
						<form>
							<input type="submit" value="book now" />
						</form>
					</div>
				</li>
				<div class="clear"></div>
			</ul>
		</div>
		<div class="clear"></div>
		</div>
	</div>
	<div class="grids_of_3">
	
	
	</div>
	<c:if test="${not empty categoryList}">
	<c:forEach var="category" items="${categoryList}">
		<div class="grid1_of_3">
		    <div class="grid1_of_3_img">
				<a href="${contextPath}/details?catid=${category.catid}">
					<img src="${contextPath}/resources/web/images/pic1.jpg" alt="" />
					<span class="next"> </span>
				</a>
			</div>
			<h4><a href="#"><span>${category.categoryName}</span><span>${category.cost}</span></a></h4>
			<p>${category.descrip}</p>
		</div>
	</c:forEach>
	</c:if> 
</div>
</div>
<%@include file="/WEB-INF/views/footer.jsp" %>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>