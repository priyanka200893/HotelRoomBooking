<%@include file="/WEB-INF/views/header.jsp" %>
<!--start main -->
<div class="main_bg">
<div class="wrap">
	<div class="main">
		<div class="details">
			<div class="det_pic">
				  <img src="${contextPath}/resources/uploads/${catid}${image}" alt="" />
			</div>
			<div class="det_text">
				<p class="para">${categoryDescription}</p>
				<p class="para">${categoryDescription}</p>
		
				<div>
					<c:if test="${not empty RoomsList}">
						<c:forEach var="room" items="${RoomsList}">
							<p>Room Id: ${room.roomNo}</p>
						</c:forEach>
					
					</c:if>
				</div>				
				<div class="read_more">
					 <a href="details.html">read more</a>
				</div>
				
				<h3>User Reviews: </h3>
				<table>
					<tr>
					<th><a href="${contextPath}/usrlinks?loc=AddReview">Add Review</a></th>
					</tr>
					<tr>
					<th><a href="${contextPath}/usrlinks?loc=ViewReviewsDispatch">Users View Reviews</a></th>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
</div>		
<!--start footer -->
<%@include file="/WEB-INF/views/footer.jsp" %>		
</body>
</html>