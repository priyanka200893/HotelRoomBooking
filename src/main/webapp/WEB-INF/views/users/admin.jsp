<%@include file="/WEB-INF/views/header.jsp" %>

<div id="container">
        
   <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome ${pageContext.request.userPrincipal.name} | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>

  </c:if>
	
</div>

<div id="links">
	 <table align="left">
		<tr>
		<th><a href="${contextPath}/admin/cat?loc=Category">Add Category</a></th>
		</tr>
		<tr>
		<th><a href="${contextPath}/admin/cat?loc=EditCategory">Edit Category</a></th>
		</tr>
		<tr>
		<th><a href="${contextPath}/admin/cat?loc=Facility">Add Facility</a></th>
		</tr>
		<tr>
		<th><a href="${contextPath}/admin/cat?loc=FacilityDispatch">View Facility</a></th>
		</tr>
		<tr>
		<th><a href="${contextPath}/admin/cat?loc=AddRooms">Add Rooms</a></th>
		</tr>
		<tr>
		<th><a href="${contextPath}/admin/cat?loc=RoomsDispatch">View Rooms</a></th>
		</tr>
		<tr>
		<!-- <th><a href="${contextPath}/admin/cat?loc=AddReview">Add Review</a></th> -->
		<th><a href="${contextPath}/admin/cat?loc=ReviewDispatch">Approve/Reject Review</a></th>
		</tr>
	</table>
</div>


<div class="main_bg">
<div class="wrap">
	<!--start grids_of_3 -->
	<div class="grids_of_3">
		<div class="clear"></div>
	</div>	
</div>
</div>		
<!--start main -->
<%@include file="/WEB-INF/views/footer.jsp" %>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>