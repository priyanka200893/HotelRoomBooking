<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.zot.Admin.facility.*"%>>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/Css/Overall.css" type="text/css" />
</head>
<body>
	<table>
		<tr>
			<td><%@include file="/WEB-INF/views/LogoPage.jsp"%>
			</td>
		</tr>
	</table>
	<hr>


	<table align="center">
		<tr>
			<td class="linkpos"><%@include file="/WEB-INF/views/Links.jsp"%></td>
			<td><form action="${contextPath}/admin/UpdateFacility?${_csrf.parameterName}=${_csrf.token}" method="POST">
					<table>
						<caption>Edit Facility</caption>
						<tr>
							<td><input type="hidden" name="facid" value="${facid}" /></td>
						</tr>
						<tr>
							<td>Facility Name</td>
							<td><input type="text" name="facname" value="${facname}" /></td>
							<td><input type="hidden" name="checkfacname" value="${facname}" /></td>
							<td>${msg}</td>
							<td>${facnameerror}</td>
						</tr>
						<tr>
							<td><input type="submit" name="sub" value="Update Facility" /></td>
						</tr>
					</table>
				</form></td>
		</tr>

	</table>

</body>
</html>